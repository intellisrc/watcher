package com.intellisrc.watcher.obj;

import java.awt.Dimension;
import java.awt.image.BufferedImage;
import java.io.FileInputStream;
import java.util.ArrayList;

/**
 *
 * @author Alberto Lepe <lepe@intellisrc.com>
 */
public class W {
	public static final int NOLOG = 0;
	public static final int LOG = 1;
	public static final int ALERT = 2;
	public static ArrayList<wEvent> events = new ArrayList();
	public static ArrayList<CameraConfig> cameras = new ArrayList();
	public static int selCam = -1; //Value set in Auth
	public static wProperties props;
	
	public static void loadProps() {
		props = new wProperties();
		try {
			props.load(new FileInputStream("watch.cfg"));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	//--------------- CAMERAS AND CONFIG ----------------
	public static CameraConfig getConfig() {
		return getConfig(selCam);
	}
	
	public static CameraConfig getConfig(int cid) {
		for(CameraConfig cc : cameras) {
			if(cc.cid == cid) {
				return cc;
			}	
		}
		return null;
	}
	
	public static Camera getCamera() {
		return getCamera(selCam);
	}
	public static Camera getCamera(int CameraId) {
		return (cameras.size() > 0 && getConfig(CameraId) != null) ? getConfig(CameraId).camera : null;
	}

	public static void setCamera(Camera config) {
		getConfig(selCam).camera = config;
	}
	
	public static wAreas getAreas(int CameraId) {
		return (cameras.size() > 0 && getConfig(CameraId) != null) ? getConfig(CameraId).areas : null;
	}

	public static void setAreas(ArrayList areas) {
		getConfig(selCam).areas.replaceAreas(areas);
	}

	//------------ EVENTS --------------
    public static void setEvent(int CameraId, int AreaId, BufferedImage img) {
		for(int r = 0; r < W.getConfig(CameraId).areas.get(AreaId).getRules().size(); r++) {
			wEvent event = searchEvent(CameraId, AreaId, r);
			if(event == null) {
				event = new wEvent(CameraId, AreaId, r);
				events.add(event);
				event.start();
			}
			event.detected(img);
		}
		img.flush();
    }

    public static void noEvent(int CameraId, int AreaId, BufferedImage img) {
		for(int r = 0; r < W.getConfig(CameraId).areas.get(AreaId).getRules().size(); r++) {
			wEvent event = searchEvent(CameraId, AreaId, r);
			if(event != null) {
				event.notDetected(img);
				if(event.isDone()) {
					events.remove(event);
				}
			}
		}
		img.flush();
    }

    private static wEvent searchEvent(int CameraId, int AreaId, int RuleId) {
        for(wEvent event : events) {
            if(event.CameraId == CameraId && event.AreaId == AreaId && event.RuleId == RuleId) {
                return event;
            } 
        }
        return null;
    }

	public static int AreaStatus(int CameraId, int AreaId) {
		CameraConfig cc = W.getConfig(CameraId);
		int topStatus = NOLOG;
			for(int r = 0; r < cc.areas.get(AreaId).getRules().size(); r++) {
				wRule rule = cc.areas.get(AreaId).getRule(r);
				if(rule.always) { //FIXME: it was if (rule.from < now > rule.to)
					if(rule.alert) topStatus = ALERT;
					else if(rule.log && topStatus < ALERT) topStatus = LOG;
				}
			}
		return topStatus;
	}
	/**
	 * Helper function to retrieve Dimensions from String
	 * @param value : e.g. 1024x768
	 * @return Dimension
	 */
	public static Dimension getDim(String value) {
		String[] s = value.split("x");
		return new Dimension(Integer.parseInt(s[0]),Integer.parseInt(s[1]));
	}
}
