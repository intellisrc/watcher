package com.intellisrc.watcher.obj;

import java.io.Serializable;
import java.net.URL;

/**
 * Structure of all the information required by camera
 * This object IS stored in DB
 * @author Alberto Lepe <lepe@intellisrc.com>
 */
public class Camera implements Serializable {
	public URL 		url 		= null;
	public int	  	fps 		= 20; //Real value is divided by 10
	public String 	size 		= "640x480";
	public boolean 	invert 		= false;
	// Export Settings:
	public int		exp_quality = wSaver.NORMAL;
	public boolean  exp_onlyevent = true;
	public boolean 	exp_resize 	= true;
	public String 	exp_size 	= "320x240";
	public String   exp_path	= "logs";
	public int		exp_buffer	= 0;
}