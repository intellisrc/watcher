package com.intellisrc.watcher.obj;

import com.intellisrc.watcher.cv.Watcher;
import com.intellisrc.watcher.swing.*;
import java.awt.Dimension;

/**
 * 
 * @author Alberto Lepe <lepe@intellisrc.com>
 */
public class CameraConfig {
	public int cid;
	public String model;
	public String title;
	public wAreas areas;
	public Camera camera;

	public int fps_mult;
	public String def_user;
	public String def_pass;
	public String resolutions;
	public String grab;
	public String up;
	public String down;
	public String left;
	public String right;
	public String home;
	public String in;
	public String out;
	public String nightvision_on;
	public String nightvision_off;
	public String contrastp;
	public String contrastm;
	public String backlight_on;
	public String backlight_off;

	//Added Dynamically:
	public Dimension real_size; //Real size is obtained from image size during grab (sometimes may differ)
	public wIFCamera viframe; //Added on connect
	public wMenuItem vmenu; //Added on Menu
	public Watcher watcher;
	public VPanel video;
	public wIFLog logger = null;
}
