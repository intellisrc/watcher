package com.intellisrc.watcher.obj;

import com.intellisrc.watcher.utils.Sound;
import java.awt.image.BufferedImage;
import java.io.File;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
//import org.apache.commons.collections.Buffer;
//import org.apache.commons.collections.buffer.UnboundedFifoBuffer;
import org.apache.commons.io.FileUtils;
import org.apache.commons.mail.DefaultAuthenticator;
import org.apache.commons.mail.EmailAttachment;
import org.apache.commons.mail.MultiPartEmail;

/**
 *
 * @author Alberto Lepe <lepe@intellisrc.com>
 */
public class wEvent {
    private wRule rule;
    public final int CameraId;
    public final int AreaId;
    public final int RuleId;
    private int LogId = 0;
    private LocalDateTime start = LocalDateTime.now();
    private LocalDateTime stop = LocalDateTime.now();
    private boolean done;
	private wSaver saver;
	//private Buffer buffer = new UnboundedFifoBuffer();
	//private Buffer timebuffer = new UnboundedFifoBuffer();

    public wEvent(int cid, int aid, int rid) {
        rule = W.getConfig(cid).areas.get(aid).getRule(rid);
		CameraId = cid;
		AreaId = aid;
		RuleId = rid;
        if(!rule.log) {
			done = true;
		} else {
			saver = new wSaver(CameraId);
		}
    }

    public void start() {
        start = LocalDateTime.now();
    }

    public void stop() {
        stop = LocalDateTime.now();
    }

    public boolean isDone() {
        return done; //Object will be destroyed
    }

	private void done() {
		update("終わり");
		done = true;
		wDB.updLog(LogId, stop);
		/*
		new Thread(new Runnable() { 
		  public void run() {
			while(!buffer.isEmpty()) {
				saver.save((BufferedImage)buffer.remove(), LogId, (LocalDateTime)timebuffer.remove());
			}
		  }
		}).start();*/
	}
    
    public void notDetected(BufferedImage img) {
		if(stop.until(LocalDateTime.now(), ChronoUnit.SECONDS)  > W.props.getIntProperty("event.group")) {
            done();
        } else if(LogId > 0) {
			saver.save(img, LogId, LocalDateTime.now());
			update("待中");
			/*
			buffer.add(img);
			timebuffer.add(new LocalDateTime());*/
			img.flush();
		}
    }

    public void detected(BufferedImage img) {
        stop();
		if(start.until(LocalDateTime.now(), ChronoUnit.SECONDS) > rule.min) {
		    LocalDateTime now = LocalDateTime.now();
			if(rule.always) { //FIXME: it was if (rule.from < now > rule.to)
				if(LogId > 0) {
					update("更新中");
				} else {
					LogId = insert();
					alert(img);
				}
			}
			saver.save(img, LogId, LocalDateTime.now());
			/*
			buffer.add(img);
			timebuffer.add(new LocalDateTime());
			 * 
			 */
			img.flush();
        }
    }

    private void update(String status) {
		if(!done) { //TODO: asynch causing problems?
			//Update DB value for event time
			//using positive
			try { //TODO: temporally... something wrong is going on
			CameraConfig cc = W.getConfig(CameraId);
			String[] prev = cc.logger.getLog(LogId);
			prev[2] = stop.format(DateTimeFormatter.ofPattern("HH:mm:ss"));
			prev[3] = String.valueOf(start.until(LocalDateTime.now(), ChronoUnit.SECONDS));
			prev[6] = status;
			cc.logger.setLog(LogId, prev);
			cc.logger.repaint();
			} catch(Exception e) {
				e.printStackTrace();
			}
		}
    }

    private int insert() {
		int logid = 0;
        //Insert into DATABASE
        //and set LogID = insert_id()
        if(rule.log) {
			CameraConfig cc = W.getConfig(CameraId);
			long seconds = start.until(LocalDateTime.now(), ChronoUnit.SECONDS);
			String zoneName = cc.areas.get(AreaId).name; 
			String ruleName = cc.areas.get(AreaId).getRule(RuleId).title;
			logid = wDB.addLog(start, stop, CameraId, AreaId, RuleId);
			cc.logger.addLog(new String[]{
					String.valueOf(logid),
					start.format(DateTimeFormatter.ofPattern("HH:mm:ss")),
					stop.format(DateTimeFormatter.ofPattern("HH:mm:ss")),
					String.valueOf(seconds),
					zoneName,
					ruleName,
					"新規"}); //LANG
			cc.logger.repaint();
        }
		return logid; 
    }

	private void alert(BufferedImage img) {
		if(rule.alert) {
			String to = rule.email.getAddress();
			if(to.length() > 0) {
				CameraConfig cc = W.getConfig(CameraId);
				String host = W.props.getProperty("mail.server");
				MultiPartEmail email = new MultiPartEmail();
				if(host.indexOf(':') == -1) {
					email.setHostName(host);
				} else {
					email.setHostName(host.split(":")[0]);
					email.setSmtpPort(Integer.parseInt(host.split(":")[1]));
				}
				if(W.props.getBoolProperty("mail.ssl")) email.setSSL(true);
				String user = W.props.getProperty("mail.user");
				if(user.length() > 0) {
					String pass = W.props.getProperty("mail.pass");
					email.setAuthenticator(new DefaultAuthenticator(user, pass));
					if(W.props.getBoolProperty("mail.tls")) email.setTLS(true);
				}
				email.setSubject("Watcher Alert"); //LANG
				wSaver ws = new wSaver(cc.cid);
				ws.asVideo = false;
				ws.save(img, 0, LocalDateTime.now());
				String fname = ws.getFileName();
				EmailAttachment attachment = new EmailAttachment();
				attachment.setPath(fname);
				attachment.setDisposition(EmailAttachment.ATTACHMENT);
				attachment.setDescription(rule.title);
				attachment.setName("CameraShot-"+LogId+".jpg");
				try {
					email.setMsg("ID："+LogId+"\nカメラ："+cc.title+"\nエリア："+cc.areas.get(AreaId).name+"\nルール："+rule.title); //LANG
					email.setFrom(W.props.getProperty("mail.sender"));
					email.addTo(to);
					email.attach(attachment);
					email.send();
				} catch(Exception e) {
					e.printStackTrace();
				}
				FileUtils.deleteQuietly(new File(fname));
			}
			if(rule.sound > 0) {
				Sound.play(rule.soundFile);	
			}
		}
	}
}
