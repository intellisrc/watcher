package com.intellisrc.watcher.obj;

import java.io.Serializable;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;

/**
 * This object IS stored in DB
 * @author Alberto Lepe <lepe@intellisrc.com>
 */
public class wRule implements Serializable {
	public boolean log = false;
	public boolean alert = false;
	public boolean always = true;
	public long from = 0;
	public long to   = 86399999L; //24Hours in milliseconds (-1 sec)
	public int min = 0;
	public String title = "使用禁止";
	public InternetAddress email;
	public int sound = 0;
	public String soundFile;

	/**
	 * No LOG
	 * 
	 * @param title 
	 */
	public wRule(String title){
		this.title = title;
		//Default values
	}
	// 
	/**
	 * LOG ONLY - ALWAYS
	 * @param title
	 * @param min 
	 */
	public wRule(String title, int min){
		this.min = min;
		this.title = title;
		this.log = true;
	}
	/**
	 * LOG ONLY - INTERVAL
	 * @param title
	 * @param min
	 * @param from
	 * @param to 
	 */
	public wRule(String title, int min, long from, long to){
		this.from = from;
		this.to = to;
		this.min = min;
		this.title = title;
		this.log = true;
		this.always = false;
	}
	/**
	 * ALERT - ALWAYS - NO SOUND
	 * @param title
	 * @param min
	 * @param email
	 * @throws AddressException 
	 */
	public wRule(String title, int min, String email) throws AddressException {
		//Default values
		this.min = min;
		this.title = title;
		this.email = new InternetAddress(email);
		this.log = true;
		this.alert = true;
	}
	/**
	 * ALERT - ALWAYS - WITH SOUND (predefined)
	 * @param title
	 * @param min
	 * @param email
	 * @param isound 
	 * @throws AddressException 
	 */
	public wRule(String title, int min, String email, int isound) throws AddressException {
		//Default values
		this.min = min;
		this.title = title;
		this.email = new InternetAddress(email);
		this.log = true;
		this.alert = true;
		this.sound = isound;
		this.soundFile = getSoundFile(isound);
	}
	/**
	 * ALERT - ALWAYS - WITH SOUND
	 * @param title
	 * @param min
	 * @param email
	 * @param isound 
	 * @param file
	 * @throws AddressException 
	 */
	public wRule(String title, int min, String email, int isound, String file) throws AddressException {
		//Default values
		this.min = min;
		this.title = title;
		this.email = new InternetAddress(email);
		this.log = true;
		this.alert = true;
		this.sound = isound;
		this.soundFile = file;
	}
	/**
	 * ALERT - INTERVAL - NO SOUND
	 * @param title
	 * @param min
	 * @param email
	 * @param from
	 * @param to
	 * @throws AddressException 
	 */
	public wRule(String title, int min, String email, long from, long to) throws AddressException {
		//Default values
		this.min = min;
		this.title = title;
		this.email = new InternetAddress(email);
		this.from = from;
		this.to = to;
		this.log = true;
		this.alert = true;
		this.always = false;
	}
	/** 
	 * ALERT - INTERVAL - WITH SOUND
	 * 
	 * @param title
	 * @param min
	 * @param email
	 * @param from
	 * @param to
	 * @param isound
	 * @throws AddressException 
	 */
	public wRule(String title, int min, String email, long from, long to, int isound) throws AddressException {
		//Default values
		this.min = min;
		this.title = title;
		this.email = new InternetAddress(email);
		this.from = from;
		this.to = to;
		this.log = true;
		this.alert = true;
		this.sound = isound;
		this.soundFile = getSoundFile(isound);
		this.always = false;
	}	
	/** 
	 * ALERT - INTERVAL - WITH SOUND
	 * 
	 * @param title
	 * @param min
	 * @param email
	 * @param from
	 * @param to
	 * @param isound
	 * @param file
	 * @throws AddressException 
	 */
	public wRule(String title, int min, String email, long from, long to, int isound, String file) throws AddressException {
		//Default values
		this.min = min;
		this.title = title;
		this.email = new InternetAddress(email);
		this.from = from;
		this.to = to;
		this.log = true;
		this.alert = true;
		this.sound = isound;
		this.soundFile = file;
		this.always = false;
	}

	public static String getSoundFile(int index) {
		return "/com/intellisrc/alpha/etc/alert" +index+".wav";
	}
}