package com.intellisrc.watcher.obj;

import java.util.Properties;

/**
 *
 * @author Alberto Lepe <lepe@intellisrc.com>
 * Enables the use of booleans and integer in properties
 */
public class wProperties extends Properties {
	public void setProperty(String key, int value) {
		setProperty(key, String.valueOf(value));
	}	
	public void setProperty(String key, boolean value) {
		setProperty(key, String.valueOf(value));
	}	
	public int getIntProperty(String key) {
		return Integer.parseInt(getProperty(key));
	}	
	public boolean getBoolProperty(String key) {
		return Boolean.valueOf(getProperty(key));	
	}	
}