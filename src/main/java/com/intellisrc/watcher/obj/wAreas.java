package com.intellisrc.watcher.obj;

import java.awt.geom.GeneralPath;
import java.util.ArrayList;

/**
 *
 * @author Alberto Lepe <lepe@intellisrc.com>
 */
public class wAreas {
    private ArrayList<wArea> arealist = new ArrayList();

    public wArea add(wArea area) {
		arealist.add(area);
		return get(arealist.indexOf(area));
    }

	public wArea add(GeneralPath path, String name) {
		wArea wa = new wArea(path,name);
		wa.addRule(new wRule("デフォルト", W.props.getIntProperty("event.min")));
		return add(wa);
	}

	public ArrayList getAreas() {
		return arealist;
	}

	public void replaceAreas(ArrayList newAreas) {
		arealist = newAreas;
	}
    
    public wArea get(int id) {
		return (wArea) arealist.get(id);
    }

	public wArea first(){
		wArea wa = null;
		for(int a = 0; a < arealist.size(); a++){  //TODO: convert to for ( .. : .. )
			wa = get(a);
			break;
		}
		return wa;
	}
	
	public void del(wArea area) {
		arealist.remove(area);
	}
	
    public void del(int id) {
		arealist.remove(id);
    }

	public int size() {
		return arealist.size();
	}
	
    public wArea search(int x, int y) {
		wArea wa = null;
		for(int a = 0; a < arealist.size(); a++){  //TODO: convert to for ( .. : .. )
			if(get(a).path.contains(x,y)) {
				wa = get(a);
				break;
			}
		}
		return wa;
    } 
}