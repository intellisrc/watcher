package com.intellisrc.watcher.obj;

import java.awt.geom.GeneralPath;
import java.io.Serializable;
import java.util.ArrayList;

/**
 *
 * @author Alberto Lepe <lepe@intellisrc.com>
 */
public class wArea implements Serializable {
	public GeneralPath path = null;
	public String name = "";
	private ArrayList<wRule> rules = new ArrayList();

	public wArea(GeneralPath path, String name) {
		this.path = path;
		this.name = name;
	}

	public void addRule(wRule rule) {
		rules.add(rule);
	}

	public wRule getRule(int id){
		return rules.size() > id ? rules.get(id) : null;
	}

	public ArrayList<wRule> getRules() {
		return rules;
	}
	
	public void delRule(int id) {
		rules.remove(id);
	}

	public void updRule(int id, wRule rule) {
		rules.set(id, rule);
	}

	public int numRules(){
		return rules.size();
	}
}