package com.intellisrc.watcher.obj;

import com.intellisrc.watcher.ui.WUI;

import java.io.File;
import java.io.InputStream;
import java.sql.*;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 *
 * @author Alberto Lepe <lepe@intellisrc.com>
 */
public class wDB {
	private static Connection conn = null;
	/**
	 * Connect to the DB
	 */
	public static void connect() {
		try {
		    Class.forName("org.h2.Driver");
		    String path = System.getProperty("user.dir") + File.separator + "watch.db";
            conn = DriverManager.getConnection("jdbc:h2:"+path+";USER=watcheroot;PASSWORD=VhJ04bKXwdgOMT"); //TODO: set in config
		    if(!new File(path).exists()) {
		        setup();
            }
		} catch(Exception e) {
			e.printStackTrace();
		}
	}
	/**
	 * Disconnect from the DB
	 */
	public static void disconnect() {
		try {
			conn.close();		
		} catch(Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Initialize DB 
	 */
	public static void setup() {
		InputStream inputStream = WUI.getResourceAsStream("etc/db.sql");
		String sql = new Scanner(inputStream).useDelimiter("\\A").next();
		try {
			Statement stat = conn.createStatement();
			stat.execute(sql.replaceAll("(/\\*([^*]|[\\r\\n]|(\\*+([^*/]|[\\r\\n])))*\\*+/)|(//.*)", ""));
		} catch (SQLSyntaxErrorException e) {
			System.out.println(e.getCause());
			System.out.println(e.getMessage());
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static boolean login(String user, String pass) {
		connect();
		boolean login = false;
		try {
			PreparedStatement prep = conn.prepareStatement("SELECT `defcam` FROM `users` WHERE `user` = ? AND `pass` = HASH('SHA256',STRINGTOUTF8(?),10);");
			prep.setString(1, user);
			prep.setString(2, pass);
			ResultSet rs = prep.executeQuery();
			if(rs.next()) {
				W.selCam = rs.getInt(1);
				login = true;
			}
			rs.close();		
		} catch (SQLSyntaxErrorException e) {
			System.out.println(e.getCause());
			System.out.println(e.getMessage());
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		disconnect();
		return login;
	}
	/**
	 * Returns the last id
	 * @param table
	 * @param field 
	 * @return int
	 */
	private static int getLastId(String table, String field) {
		connect();
		int lastid = 0;
		try {
			Statement stat = conn.createStatement();
			ResultSet rs = stat.executeQuery("SELECT MAX(`"+field+"`) FROM `"+table+"`");
			if(rs.next()){
				lastid = rs.getInt(1);
			}
			rs.close();
		} catch (SQLSyntaxErrorException e) {
			System.out.println(e.getCause());
			System.out.println(e.getMessage());
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		disconnect();	
		return lastid;
	}

	public static int create(String title, int ModelId, Camera cc) {
		connect();
		try {
			PreparedStatement prep = conn.prepareStatement("INSERT INTO cams SET mid = ?, title = ?, areas = ?, config = ?");
			prep.setInt(1, ModelId);
			prep.setString(2, title);
			prep.setObject(3, new ArrayList());
			prep.setObject(4, cc);
			prep.executeUpdate();
		} catch (SQLSyntaxErrorException e) {
			System.out.println(e.getCause());
			System.out.println(e.getMessage());
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		disconnect();
		load(); //TODO: check if the order is kept after insert, if not, then just append to the ArrayList
		return getLastId("cams","cid");
	}

	public static void destroy(int cid) {
		connect();
		try {
			PreparedStatement prep = conn.prepareStatement("DELETE FROM cams WHERE cid = ?");
			prep.setInt(1, cid);
			prep.executeUpdate();
		} catch (SQLSyntaxErrorException e) {
			System.out.println(e.getCause());
			System.out.println(e.getMessage());
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		disconnect();			
		load(); //TODO: check if the order is kept after insert, if not, then just append to the ArrayList
	}

	public static void saveCfg(int fid) {
		connect();
		try {
			PreparedStatement prep = conn.prepareStatement("UPDATE cams SET config = ? WHERE cid = ?");
			Camera cam = W.getCamera();
			int cid = W.getConfig().cid;
			prep.setObject(1, cam);
			prep.setInt(2, cid);
			prep.executeUpdate();
		} catch (SQLSyntaxErrorException e) {
			System.out.println(e.getCause());
			System.out.println(e.getMessage());
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		disconnect();		
	}
	
	public static void save(int fid) {
		connect();
		try {
			PreparedStatement prep = conn.prepareStatement("UPDATE cams SET areas = ? WHERE cid = ?");
			ArrayList areas = W.getAreas(fid).getAreas();
			int cid = W.getConfig().cid;
			prep.setObject(1, areas);
			prep.setInt(2, cid);
			prep.executeUpdate();
		} catch (SQLSyntaxErrorException e) {
			System.out.println(e.getCause());
			System.out.println(e.getMessage());
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		disconnect();		
	}

	public static void load() {
		connect();
		ArrayList cams = new ArrayList();
		try {
			Statement stat = conn.createStatement();
			ResultSet rs = stat.executeQuery("SELECT * FROM cams c JOIN models m ON (c.mid = m.mid) JOIN kamkonf k ON (m.kid = k.kid)");
			while(rs.next()){
				CameraConfig cc = new CameraConfig();
				cc.cid = rs.getInt("cid");
				cc.camera = (Camera) rs.getObject("config");
				cc.areas = new wAreas();
				cc.areas.replaceAreas((ArrayList) rs.getObject("areas"));
				cc.model = rs.getString("model");
				cc.title = rs.getString("title");
				cc.fps_mult = rs.getInt("fps_mult");
				cc.def_user = rs.getString("def_user");
				cc.def_pass = rs.getString("def_pass");
				cc.resolutions = rs.getString("resolutions");
				cc.grab = rs.getString("grab");
				cc.up = rs.getString("up");
				cc.down = rs.getString("down");
				cc.left = rs.getString("left");
				cc.right = rs.getString("right");
				cc.home = rs.getString("home");
				cc.in = rs.getString("in");
				cc.out = rs.getString("out");
				cc.nightvision_on = rs.getString("nightvision_on");
				cc.nightvision_off = rs.getString("nightvision_off");
				cc.contrastp = rs.getString("contrastp");
				cc.contrastm = rs.getString("contrastm");
				cc.backlight_on = rs.getString("backlight_on");
				cc.backlight_off = rs.getString("backlight_off");
				cams.add(cc);
			}
			rs.close();		
		} catch (SQLSyntaxErrorException e) {
			System.out.println(e.getCause());
			System.out.println(e.getMessage());
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		W.cameras = cams;
		disconnect();
	}
	/**
	 * Returns from DB the configuration of a camera model
	 * @param model
	 * @return CameraConfig : without customization values like camera or areas
	 */
	public static CameraConfig getModelConfig(String model) {
		connect();
		CameraConfig cc = null;
		try {
			PreparedStatement prep = conn.prepareStatement("SELECT k.* FROM models m JOIN kamkonf k ON (m.kid = k.kid) WHERE model = ?");
			prep.setString(1, model);
			ResultSet rs = prep.executeQuery();
			if(rs.next()){
				cc = new CameraConfig();
				cc.fps_mult = rs.getInt("fps_mult");
				cc.def_user = rs.getString("def_user");
				cc.def_pass = rs.getString("def_pass");
				cc.resolutions = rs.getString("resolutions");
				cc.grab = rs.getString("grab");
				cc.up = rs.getString("up");
				cc.down = rs.getString("down");
				cc.left = rs.getString("left");
				cc.right = rs.getString("right");
				cc.home = rs.getString("home");
				cc.in = rs.getString("in");
				cc.out = rs.getString("out");
				cc.nightvision_on = rs.getString("nightvision_on");
				cc.nightvision_off = rs.getString("nightvision_off");
				cc.contrastp = rs.getString("contrastp");
				cc.contrastm = rs.getString("contrastm");
				cc.backlight_on = rs.getString("backlight_on");
				cc.backlight_off = rs.getString("backlight_off");
			}
			rs.close();		
		} catch (SQLSyntaxErrorException e) {
			System.out.println(e.getCause());
			System.out.println(e.getMessage());
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		disconnect();
		return cc;
	}
	
	public static int getModelId(String model) {
		connect();
		int mid = 0;
		try {
			PreparedStatement prep = conn.prepareStatement("SELECT mid FROM models WHERE model = ?");
			prep.setString(1, model);
			ResultSet rs = prep.executeQuery();
			if(rs.next()){
				mid = rs.getInt(1);
			}
			rs.close();		
		} catch (SQLSyntaxErrorException e) {
			System.out.println(e.getCause());
			System.out.println(e.getMessage());
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		disconnect();
		return mid;	
	}
	
	public static List getCamModels() {
		connect();
		List cams = new ArrayList();
		try {
			Statement stat = conn.createStatement();
			ResultSet rs = stat.executeQuery("SELECT model FROM models");
			while(rs.next()){
				cams.add(rs.getString(1));
			}
			rs.close();		
		} catch (SQLSyntaxErrorException e) {
			System.out.println(e.getCause());
			System.out.println(e.getMessage());
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		disconnect();
		return cams;
	}
	
	public static int addLog(LocalDateTime start, LocalDateTime stop, int CameraId, int AreaId, int RuleId) {
		connect();
		try {
			PreparedStatement prep = conn.prepareStatement("INSERT INTO logs SET `start` = ?, `stop` = ?, `cid` = ?, `aid` = ?, `rid` = ?");
			prep.setTimestamp(1, Timestamp.valueOf(start));
			prep.setTimestamp(2, Timestamp.valueOf(stop));
			prep.setInt(3, CameraId);
			prep.setInt(4, AreaId);
			prep.setInt(5, RuleId);
			prep.executeUpdate();
		} catch (SQLSyntaxErrorException e) {
			System.out.println(e.getCause());
			System.out.println(e.getMessage());
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		disconnect();
		return getLastId("logs","lid");
	}

	public static void updLog(int LogId, LocalDateTime stop) {
		connect();
		try {
			PreparedStatement prep = conn.prepareStatement("UPDATE logs SET stop = ? WHERE lid= ?");
			prep.setTimestamp(1, Timestamp.valueOf(stop));
			prep.setInt(2, LogId);
			prep.executeUpdate();
		} catch (SQLSyntaxErrorException e) {
			System.out.println(e.getCause());
			System.out.println(e.getMessage());
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		disconnect();		
	}

	public static ArrayList<String[]> getLog(int CameraId) {
		connect();
		ArrayList<String[]> logs = new ArrayList();
		try {
			PreparedStatement prep = conn.prepareStatement("SELECT * FROM logs WHERE cid = ? AND start > ?");
			prep.setInt(1, CameraId);
			prep.setTimestamp(2, Timestamp.valueOf(LocalDateTime.now().minusDays(3)));
			ResultSet rs = prep.executeQuery();
			while(rs.next()){
				int RuleId = rs.getInt("rid");
				int AreaId = rs.getInt("aid");
				CameraConfig cc = W.getConfig(CameraId);
				wArea wa = cc.areas.get(AreaId);
				if(wa != null) {
					wRule wr = wa.getRule(RuleId);
					if(wr != null) {
						String areaName = wa.name;
						String ruleName = wr.title;
						LocalDateTime start = rs.getTimestamp("start").toLocalDateTime();
						LocalDateTime stop = rs.getTimestamp("stop").toLocalDateTime();
						long secs = start.until(stop, ChronoUnit.SECONDS);
						String lid = rs.getString("lid");
						logs.add(new String[]{
								lid,
								start.format(DateTimeFormatter.ofPattern("HH:mm:ss")),
								stop.format(DateTimeFormatter.ofPattern("HH:mm:ss")),
								String.valueOf(secs),
								areaName,
								ruleName
						});
					}
				}
			}
			rs.close();		
		} catch (SQLSyntaxErrorException e) {
			System.out.println(e.getCause());
			System.out.println(e.getMessage());
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		disconnect();
		return logs;	
	}

	public static void cleanLog(int days) {
		connect();
		try {
			PreparedStatement prep = conn.prepareStatement("DELETE FROM logs WHERE start < ?");
			prep.setTimestamp(1, Timestamp.valueOf(LocalDateTime.now().minusDays(days)));
			prep.executeUpdate();
		} catch (SQLSyntaxErrorException e) {
			System.out.println(e.getCause());
			System.out.println(e.getMessage());
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		disconnect();			
	}
}
