package com.intellisrc.watcher.obj;

import com.intellisrc.watcher.cv.MJpegEncoder;
import com.intellisrc.watcher.utils.ImgResizer;
import com.intellisrc.watcher.utils.ImgSaver;
import java.awt.Dimension;
import java.awt.image.BufferedImage;
import java.io.File;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import org.apache.commons.io.FileUtils;

/**
 *
 * @author Alberto Lepe <lepe@intellisrc.com>
 * Exports images to files 
 */
public class wSaver {
	public static final int LOW 	= 0;
	public static final int NORMAL  = 1;
	public static final int HIGH 	= 2;
	public boolean asVideo = true;
	private int quality = NORMAL;
	private CameraConfig cc;
	private String baseDirName;
	private MJpegEncoder mjpeg;
	private String file;

	public wSaver(int cid) {
		this.cc = W.getConfig(cid);
		this.quality = cc.camera.exp_quality;
		this.baseDirName = cc.camera.exp_path;
	}

	public void save(BufferedImage img, int logid, LocalDateTime time) {
		int resQlty = ImgResizer.GOOD;
		switch(quality) {
			case LOW: 	 resQlty = ImgResizer.FAST; break;
			case HIGH: 	 resQlty = ImgResizer.BEST; break;
		}
		Dimension rsize = W.getDim(cc.camera.exp_size);
		//Dimension csize = W.getDim(cc.camera.size);
		//if(rsize.width < csize.width && rsize.height < csize.width) {
			img = ImgResizer.resize(img, rsize.width, rsize.height, resQlty);
		//}
		try {
			File dir = new File(baseDirName);
			if(!dir.exists()) dir.mkdir();
			dir = new File(baseDirName + File.separator + cc.cid);
			if(!dir.exists()) dir.mkdir();
			dir = new File(baseDirName + File.separator + cc.cid + File.separator + time.format(DateTimeFormatter.ofPattern("yyMMdd")));
			if(!dir.exists()) dir.mkdir();
			
			int saveQlty = 0;
			switch(quality) {
				case LOW: saveQlty = 80; break;
				case HIGH: saveQlty = 90; break;
			}
			if(asVideo) {
				file = dir.getAbsolutePath() + File.separator + logid + ".mjpg";
				if(mjpeg == null) mjpeg = new MJpegEncoder(file);
				mjpeg.export(ImgSaver.convert(img, "jpg", saveQlty), time.format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss.SSS")));
			} else {
				file = dir.getAbsolutePath() + File.separator + logid + "-" + time.format(DateTimeFormatter.ofPattern("HHmmss-SSS"))+".jpg";
				ImgSaver.save(img, new File(file), saveQlty);
			}
			img.flush(); 
		} catch (Exception e) {
			e.printStackTrace();	
		}
	}

	public static void clean(int days){
		for(int c = 0; c < W.cameras.size(); c++) {
			CameraConfig cc = W.cameras.get(c);
			Camera cam = cc.camera;
			String bDirName = cam.exp_path;
			 File directory = new File(bDirName + File.separator + cc.cid);  
			 if(directory.exists()){  
				 File[] listFiles = directory.listFiles();              
				 long purgeTime = System.currentTimeMillis() - (days * 24 * 60 * 60 * 1000);  
				 for(File listFile : listFiles) {  
					 if(listFile.lastModified() < purgeTime) {  
						try {
							FileUtils.deleteDirectory(listFile);
						} catch(Exception e){
							e.printStackTrace();
						} 
					 }  
				 }  
			 } else {  
				 
			 }  
		}
	}

	public String getFileName() {
		return file;
	}
}