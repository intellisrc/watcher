/**
 * WUI.java
 * <p>
 * Created on Nov 29, 2011, 2:55:02 PM
 */
package com.intellisrc.watcher.ui;

import com.intellisrc.watcher.obj.*;
import com.intellisrc.watcher.swing.*;
import com.intellisrc.watcher.cv.*;
import com.intellisrc.watcher.utils.*;
import org.apache.commons.io.FileUtils;

import java.awt.Color;
import java.awt.Component;
import java.awt.Desktop;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Image;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URL;
import java.sql.Timestamp;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import javax.swing.DefaultListModel;
import javax.swing.JInternalFrame;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTable;

/**
 *
 * @author Alberto Lepe <lepe@intellisrc.com>
 */
public class WUI extends javax.swing.JFrame {
    private Font largeFont = new Font("HGMaruGothicMPRO", Font.PLAIN, 14);
	/*
	private Font regularFont = new Font("HGMaruGothicMPRO", Font.PLAIN, 12);
	private Font smallFont = new Font("HGMaruGothicMPRO", Font.PLAIN, 10);
	private Font generalFont = new Font("Arial", Font.PLAIN, 11);
	private Font logoFont = new Font("Arial", Font.ITALIC + Font.BOLD, 12);
	*/

    /** Creates new form WUI */
    public WUI() {
        initComponents();
        this.setLocationRelativeTo(getRootPane()); //To center
        W.loadProps();
        wDB.load();
        for (int c = 0; c < W.cameras.size(); c++) {
            CameraConfig cc = W.cameras.get(c);
            addCameraInMenu(cc.title, cc.cid, false);
            addIFLog(cc);
            cc.logger.importLogs(wDB.getLog(cc.cid));
        }
        List<Image> icons = new ArrayList();
        icons.add(new javax.swing.ImageIcon(getResource("icons/radiolocator.png")).getImage());
        icons.add(new javax.swing.ImageIcon(getResource("images/radiolocator.png")).getImage());
        this.setIconImages(icons);
        centerFrame(ifPlayer);
    }

    private void openURL(String key) {
        String url = W.props.getProperty(key);
        if (Desktop.isDesktopSupported()) {
            Desktop desktop = Desktop.getDesktop();
            if (desktop.isSupported(Desktop.Action.BROWSE)) {
                try {
                    URI uri = new java.net.URI(url);
                    desktop.browse(uri);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else {
                JOptionPane.showMessageDialog(this, "URL: " + url);
            }
        } else {
            JOptionPane.showMessageDialog(this, "URL: " + url);
        }
    }

    private void centerFrame(JInternalFrame JIF) {
        Dimension desktopSize = jDesktopPane.getSize();
        Dimension JIFSize = JIF.getSize();
        int left = (desktopSize.width - JIFSize.width) / 2;
        int top = (desktopSize.height - JIFSize.height) / 2;
        JIF.setLocation(left < 0 ? 0 : left, top < 0 ? 0 : top);
    }

    private void addCameraInMenu(String label, final int cid, boolean connect) {
        wMenuItem jMenuItem = new wMenuItem();
        jMenuItem.setFont(largeFont);
        jMenuItem.setText(label);
        jMenuItem.setConnect(connect);
        jMenuItem.addActionListener(evt -> {
            fixMenu(cid);
            W.getConfig(cid).vmenu.toggle();
            if (W.getConfig(cid).vmenu.isConnected()) {
                addIFVideo(cid);
            } else {
                W.getConfig(cid).watcher.quit();
                W.getConfig(cid).viframe.dispose();
            }
        });
        mnCam.add(jMenuItem);
        W.getConfig(cid).vmenu = jMenuItem;
        if (connect) addIFVideo(cid);
    }

    private void addIFLog(final CameraConfig cc) {
        final JTable tLog;
        JScrollPane jScrollPane1;
        jScrollPane1 = new JScrollPane();
        tLog = new JTable();
        cc.logger = new wIFLog();
        cc.logger.table = tLog;

        cc.logger.setClosable(true);
        cc.logger.setDefaultCloseOperation(javax.swing.WindowConstants.HIDE_ON_CLOSE);
        cc.logger.setIconifiable(true);
        cc.logger.setMaximizable(true);
        cc.logger.setResizable(true);
        cc.logger.setTitle(cc.title + "のログ");

        tLog.setFont(largeFont); // NOI18N
        tLog.setModel(new javax.swing.table.DefaultTableModel(
                new Object[][]{

                },
                new String[]{
                        "ID", "始時間", "終時間", "経過秒", "エリア", "ルール", "ステータス"
                }
        ) {
            Class[] types = new Class[]{
                    java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.String.class
            };
            boolean[] canEdit = new boolean[]{
                    false, false, false, false, false, false, false
            };

            @Override
            public Class getColumnClass(int columnIndex) {
                return types[columnIndex];
            }

            @Override
            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit[columnIndex];
            }
        });
        tLog.setRowSelectionAllowed(true);
        tLog.getSelectionModel().addListSelectionListener(evt -> {
            if (!evt.getValueIsAdjusting() && tLog.getSelectedRow() != -1) {
                int row = tLog.getSelectedRow();
                int id = Integer.parseInt((String) tLog.getValueAt(row, 0));
                vPlayer.video = cc.camera.exp_path + File.separator + cc.cid + File.separator + LocalDate.now().format(DateTimeFormatter.ofPattern("yyMMdd")) + File.separator + id + ".mjpg";
                ifPlayer.show();
                vPlayer.play();
            }
        });
        tLog.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                cc.logger.pause = true;
            }

            public void mouseExited(java.awt.event.MouseEvent evt) {
                cc.logger.pause = false;
                cc.logger.table.clearSelection();
            }
        });
        jScrollPane1.setViewportView(tLog);

        javax.swing.GroupLayout ifLogLayout = new javax.swing.GroupLayout(cc.logger.getContentPane());
        cc.logger.getContentPane().setLayout(ifLogLayout);
        ifLogLayout.setHorizontalGroup(
                ifLogLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 607, Short.MAX_VALUE)
        );
        ifLogLayout.setVerticalGroup(
                ifLogLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 380, Short.MAX_VALUE)
        );

        cc.logger.setBounds(70, 80, 609, 416);
        jDesktopPane.add(cc.logger, javax.swing.JLayeredPane.DEFAULT_LAYER);
    }

    //TODO: remove this and fix the problem!
    private void fixMenu(int cid) {
        if (W.getConfig(cid) != null) {
            if (W.getConfig(cid).vmenu == null) { //TODO: fix vmenu being lost
                for (Component comp : mnCam.getMenuComponents()) {
                    if (comp instanceof wMenuItem) {
                        wMenuItem wmi = (wMenuItem) comp;
                        if (wmi.getText().equals(W.getConfig(cid).title)) {
                            W.getConfig(cid).vmenu = wmi;
                            break;
                        }
                    }
                }
            }
        }
    }

    private void addIFVideo(int cid) {
        final wIFCamera ifVideo = new wIFCamera();
        JScrollPane vScroll = new JScrollPane();
        VPanel video = new VPanel();
        ifVideo.setClosable(true);
        ifVideo.setIconifiable(true);
        ifVideo.setMaximizable(false);
        ifVideo.setResizable(true);
        ifVideo.setVisible(true);

        vScroll.setViewportView(video);

        javax.swing.GroupLayout ifVideoLayout = new javax.swing.GroupLayout(ifVideo.getContentPane());
        ifVideo.getContentPane().setLayout(ifVideoLayout);
        ifVideoLayout.setHorizontalGroup(
                ifVideoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(vScroll, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        ifVideoLayout.setVerticalGroup(
                ifVideoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(vScroll, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        ifVideo.setBounds(20 + (100 * cid), 20 + (50 * cid), 640, 480);
        ifVideo.addInternalFrameListener(new javax.swing.event.InternalFrameListener() {
            @Override
            public void internalFrameActivated(javax.swing.event.InternalFrameEvent evt) {
                W.selCam = ifVideo.cameraId;
                mnCamRm.setEnabled(true);
                if (!ifVideo.isIcon()) {
                    mnArea.setEnabled(true);
                }
                mnTools.setEnabled(true);
                //---- Hide if exists ----
                ifCtrl.setVisible(false);
                ifRules.setVisible(false);
                ifRule.setVisible(false);
                ifExport.setVisible(false);
                fixMenu(W.selCam);
                W.getConfig(ifVideo.cameraId).vmenu.setForeground(Color.RED);
            }

            @Override
            public void internalFrameClosed(javax.swing.event.InternalFrameEvent evt) {
                W.selCam = -1;
                mnCamRm.setEnabled(false);
                mnArea.setEnabled(false);
                mnTools.setEnabled(false);
                ifCtrl.setVisible(false);
                ifRules.setVisible(false);
                ifRule.setVisible(false);
                ifExport.setVisible(false);
                if (W.getConfig(ifVideo.cameraId) != null) {
                    fixMenu(W.selCam);
                    W.getConfig(ifVideo.cameraId).vmenu.disconnect();
                    if (W.getConfig(ifVideo.cameraId).watcher != null)
                        W.getConfig(ifVideo.cameraId).watcher.quit();
                }
            }

            @Override
            public void internalFrameDeactivated(javax.swing.event.InternalFrameEvent evt) {
                mnCamRm.setEnabled(false);
                if (W.getConfig(ifVideo.cameraId) != null && W.getConfig(ifVideo.cameraId).vmenu != null)
                    W.getConfig(ifVideo.cameraId).vmenu.setForeground(Color.BLACK);
            }

            @Override
            public void internalFrameIconified(javax.swing.event.InternalFrameEvent evt) {
                W.selCam = -1;
                //---- Hide if exists ----
                ifCtrl.setVisible(false);
                ifRules.setVisible(false);
                ifRule.setVisible(false);
                ifExport.setVisible(false);
                mnArea.setEnabled(false);
                JInternalFrame.JDesktopIcon icon = ifVideo.getDesktopIcon();
                icon.setSize(new Dimension(200, icon.getSize().height));
            }

            @Override
            public void internalFrameClosing(javax.swing.event.InternalFrameEvent evt) {
            }

            @Override
            public void internalFrameDeiconified(javax.swing.event.InternalFrameEvent evt) {
            }

            @Override
            public void internalFrameOpened(javax.swing.event.InternalFrameEvent evt) {
            } //Does not trigger on create.
        });

        jDesktopPane.add(ifVideo, javax.swing.JLayeredPane.DEFAULT_LAYER);

        CameraConfig cc = W.getConfig(cid);
        ifVideo.setTitle(cc.title);
        W.selCam = ifVideo.cameraId = cid;
        cc.viframe = ifVideo; //Store the relation
        video.cid = cid;
        cc.video = video;

        Dimension videosize = W.getDim(cc.camera.size);
        video.setPreferredSize(videosize);
        ifVideo.setSize(videosize.width + 18, videosize.height + 37);
        ifVideo.setMaximumSize(new Dimension(videosize.width + 18, videosize.height + 37)); //TODO
        centerFrame(ifVideo);
        //---- ADD Action -------
        Watcher watcher = null;
        try {
            watcher = new Watcher(video, cc);
            watcher.start();
            watcher.cid = cid;
            cc.watcher = watcher;
        } catch (Exception e) {
            e.printStackTrace();
        }
        mnArea.setEnabled(true);
        mnTools.setEnabled(true);
        try { //set Focus:
            ifVideo.setSelected(true);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void refreshAreaList() {
        DefaultListModel alist = (DefaultListModel) rAreaList.getModel();
        alist.clear();
        for (int i = 0; i < W.getAreas(W.selCam).size(); i++) {
            wArea wa = W.getAreas(W.selCam).get(i);
            alist.addElement(wa.name);
        }
        rAreaList.setSelectedIndex(0);
    }

    private void refreshRuleList() {
        if (!rAreaList.isSelectionEmpty()) {
            DefaultListModel rlist = (DefaultListModel) rRuleList.getModel();
            rlist.clear();
            wArea wa = W.getAreas(W.selCam).get(rAreaList.getSelectedIndex());
            for (wRule r : wa.getRules()) {
                rlist.addElement(r.title);
            }
            rRuleList.setSelectedIndex(0);
        }
    }

    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        gbRule = new javax.swing.ButtonGroup();
        jDesktopPane = new javax.swing.JDesktopPane();
        ifCam = new javax.swing.JInternalFrame();
        jPanel3 = new javax.swing.JPanel();
        jLabel12 = new javax.swing.JLabel();
        camModel = new javax.swing.JComboBox();
        jLabel11 = new javax.swing.JLabel();
        jLabel10 = new javax.swing.JLabel();
        camInvert = new javax.swing.JCheckBox();
        jLabel7 = new javax.swing.JLabel();
        jLabel9 = new javax.swing.JLabel();
        camTitle = new javax.swing.JTextField();
        jLabel1 = new javax.swing.JLabel();
        camUser = new javax.swing.JTextField();
        camSize = new javax.swing.JComboBox();
        camIP = new javax.swing.JTextField();
        jLabel8 = new javax.swing.JLabel();
        camFPS = new javax.swing.JComboBox();
        camPass = new javax.swing.JPasswordField();
        jToolBar1 = new javax.swing.JToolBar();
        filler1 = new javax.swing.Box.Filler(new java.awt.Dimension(0, 0), new java.awt.Dimension(0, 0), new java.awt.Dimension(32767, 0));
        btnCamOK = new javax.swing.JButton();
        filler2 = new javax.swing.Box.Filler(new java.awt.Dimension(0, 0), new java.awt.Dimension(0, 0), new java.awt.Dimension(32767, 0));
        ifCtrl = new javax.swing.JInternalFrame();
        jToolBar2 = new javax.swing.JToolBar();
        filler4 = new javax.swing.Box.Filler(new java.awt.Dimension(0, 0), new java.awt.Dimension(0, 0), new java.awt.Dimension(32767, 0));
        jPanel2 = new javax.swing.JPanel();
        btnCtrlZoomOut = new javax.swing.JButton();
        btnCtrlDown = new javax.swing.JButton();
        btnCtrlZoomIn = new javax.swing.JButton();
        btnCtrlRight = new javax.swing.JButton();
        btnCtrlCenter = new javax.swing.JButton();
        btnCtrlContrastUp = new javax.swing.JButton();
        btnCtrlLeft = new javax.swing.JButton();
        btnCtrlContrastDown = new javax.swing.JButton();
        btnCtrlUp = new javax.swing.JButton();
        filler3 = new javax.swing.Box.Filler(new java.awt.Dimension(0, 0), new java.awt.Dimension(0, 0), new java.awt.Dimension(32767, 0));
        ifExport = new javax.swing.JInternalFrame();
        jPanel1 = new javax.swing.JPanel();
        expQuality = new javax.swing.JComboBox();
        expSize = new javax.swing.JComboBox();
        expOnEvent = new javax.swing.JCheckBox();
        expBuffer = new javax.swing.JComboBox();
        jSeparator2 = new javax.swing.JSeparator();
        jSeparator3 = new javax.swing.JSeparator();
        jLabel13 = new javax.swing.JLabel();
        jLabel14 = new javax.swing.JLabel();
        expPath = new javax.swing.JTextField();
        jLabel16 = new javax.swing.JLabel();
        btnChangePath = new javax.swing.JButton();
        expResize = new javax.swing.JCheckBox();
        jSeparator4 = new javax.swing.JSeparator();
        jLabel6 = new javax.swing.JLabel();
        jLabel15 = new javax.swing.JLabel();
        jComboBox1 = new javax.swing.JComboBox();
        jComboBox2 = new javax.swing.JComboBox();
        jToolBar3 = new javax.swing.JToolBar();
        filler5 = new javax.swing.Box.Filler(new java.awt.Dimension(0, 0), new java.awt.Dimension(0, 0), new java.awt.Dimension(32767, 0));
        btnExportOK = new javax.swing.JButton();
        filler6 = new javax.swing.Box.Filler(new java.awt.Dimension(0, 0), new java.awt.Dimension(0, 0), new java.awt.Dimension(32767, 0));
        ifRules = new javax.swing.JInternalFrame();
        jSplitPane1 = new javax.swing.JSplitPane();
        jPanel6 = new javax.swing.JPanel();
        jLabel20 = new javax.swing.JLabel();
        rRuleList = new javax.swing.JList();
        rAddRule = new javax.swing.JButton();
        rRmRule = new javax.swing.JButton();
        jPanel5 = new javax.swing.JPanel();
        jLabel19 = new javax.swing.JLabel();
        rAreaList = new javax.swing.JList();
        ifRule = new javax.swing.JInternalFrame();
        rRule = new javax.swing.JPanel();
        rEventPanel = new javax.swing.JPanel();
        rFromM = new javax.swing.JTextField();
        jLabel21 = new javax.swing.JLabel();
        jLabel22 = new javax.swing.JLabel();
        jLabel23 = new javax.swing.JLabel();
        jLabel24 = new javax.swing.JLabel();
        rToH = new javax.swing.JTextField();
        rToM = new javax.swing.JTextField();
        rMinU = new javax.swing.JComboBox();
        jLabel25 = new javax.swing.JLabel();
        rFromH = new javax.swing.JTextField();
        rMin = new javax.swing.JTextField();
        rAlways = new javax.swing.JCheckBox();
        jSeparator6 = new javax.swing.JSeparator();
        jLabel26 = new javax.swing.JLabel();
        rAlertPanel = new javax.swing.JPanel();
        jLabel27 = new javax.swing.JLabel();
        rEmail = new javax.swing.JTextField();
        jSeparator7 = new javax.swing.JSeparator();
        jLabel17 = new javax.swing.JLabel();
        rBtnBrowse = new javax.swing.JButton();
        rExec = new javax.swing.JTextField();
        rUrl = new javax.swing.JTextField();
        jLabel29 = new javax.swing.JLabel();
        jLabel28 = new javax.swing.JLabel();
        rSoundAlert = new javax.swing.JComboBox();
        rPlaySound = new javax.swing.JButton();
        rAddSound = new javax.swing.JButton();
        jToolBar4 = new javax.swing.JToolBar();
        filler7 = new javax.swing.Box.Filler(new java.awt.Dimension(0, 0), new java.awt.Dimension(0, 0), new java.awt.Dimension(32767, 0));
        btnRuleOK = new javax.swing.JButton();
        filler8 = new javax.swing.Box.Filler(new java.awt.Dimension(0, 0), new java.awt.Dimension(0, 0), new java.awt.Dimension(32767, 0));
        jToolBar5 = new javax.swing.JToolBar();
        filler10 = new javax.swing.Box.Filler(new java.awt.Dimension(0, 0), new java.awt.Dimension(0, 0), new java.awt.Dimension(32767, 0));
        btnNoLog = new javax.swing.JToggleButton();
        btnLogOnly = new javax.swing.JToggleButton();
        btnLogAlert = new javax.swing.JToggleButton();
        filler9 = new javax.swing.Box.Filler(new java.awt.Dimension(0, 0), new java.awt.Dimension(0, 0), new java.awt.Dimension(32767, 0));
        jLabel2 = new javax.swing.JLabel();
        rRuleName = new javax.swing.JTextField();
        ifAbout = new javax.swing.JInternalFrame();
        logo = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jLabel18 = new javax.swing.JLabel();
        version = new javax.swing.JLabel();
        ifPlayer = new javax.swing.JInternalFrame();
        vPlayer = new com.intellisrc.watcher.swing.VPlayer();
        jToolBar6 = new javax.swing.JToolBar();
        filler11 = new javax.swing.Box.Filler(new java.awt.Dimension(0, 0), new java.awt.Dimension(0, 0), new java.awt.Dimension(32767, 0));
        btnBegin = new javax.swing.JButton();
        btnRew = new javax.swing.JButton();
        btnPause = new javax.swing.JButton();
        btnPlay = new javax.swing.JButton();
        btnFwd = new javax.swing.JButton();
        btnEnd = new javax.swing.JButton();
        filler12 = new javax.swing.Box.Filler(new java.awt.Dimension(0, 0), new java.awt.Dimension(0, 0), new java.awt.Dimension(32767, 0));
        vPlayerCounter = new javax.swing.JLabel();
        ifLog = new com.intellisrc.watcher.swing.wIFLog();
        jScrollPane1 = new javax.swing.JScrollPane();
        tLog = new javax.swing.JTable();
        jMenuBar1 = new javax.swing.JMenuBar();
        mnCam = new javax.swing.JMenu();
        mnCamAdd = new javax.swing.JMenuItem();
        mnCamRm = new javax.swing.JMenuItem();
        jSeparator1 = new javax.swing.JPopupMenu.Separator();
        mnArea = new javax.swing.JMenu();
        mnAreaAdd = new javax.swing.JMenuItem();
        mnAreaRm = new javax.swing.JMenuItem();
        mnAreaView = new javax.swing.JMenuItem();
        mnTools = new javax.swing.JMenu();
        mnRule = new javax.swing.JMenuItem();
        mnLog = new javax.swing.JMenuItem();
        mnReset = new javax.swing.JMenuItem();
        mnCtrl = new javax.swing.JMenuItem();
        mnOpts = new javax.swing.JMenuItem();
        mnHelp = new javax.swing.JMenu();
        mnAbout = new javax.swing.JMenuItem();
        mnUpd = new javax.swing.JMenuItem();
        mnDoc = new javax.swing.JMenuItem();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Watcher Beta 2 - Inspeedia");

        ifCam.setClosable(true);
        ifCam.setDefaultCloseOperation(javax.swing.WindowConstants.HIDE_ON_CLOSE);
        ifCam.setIconifiable(true);
        ifCam.setTitle("カメラ追加");
        ifCam.setFocusable(false);

        jPanel3.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        jPanel3.setFocusCycleRoot(true);

        jLabel12.setFont(new java.awt.Font("HGMaruGothicMPRO", 0, 14));
        jLabel12.setText("カメラ");

        camModel.setModel(new javax.swing.DefaultComboBoxModel(new String[]{"BB-XXX999"}));
        camModel.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                camModelActionPerformed(evt);
            }
        });

        jLabel11.setFont(new java.awt.Font("HGMaruGothicMPRO", 0, 14));
        jLabel11.setText("タイトル");

        jLabel10.setFont(new java.awt.Font("HGMaruGothicMPRO", 0, 14));
        jLabel10.setText("画像解像度");

        camInvert.setFont(new java.awt.Font("HGMaruGothicMPRO", 0, 14));
        camInvert.setText("倒像");

        jLabel7.setFont(new java.awt.Font("HGMaruGothicMPRO", 0, 14));
        jLabel7.setText("ユーザ");

        jLabel9.setFont(new java.awt.Font("HGMaruGothicMPRO", 0, 14));
        jLabel9.setText("パスワード");

        camTitle.setText("Camera 1");

        jLabel1.setFont(new java.awt.Font("HGMaruGothicMPRO", 0, 14));
        jLabel1.setText("ホスト");

        camUser.setText("admin");

        camSize.setModel(new javax.swing.DefaultComboBoxModel(new String[]{"640x480"}));

        camIP.setText("192.168.20.167");
        camIP.setToolTipText("例：192.168.0.1:99");
        camIP.setPreferredSize(null);

        jLabel8.setForeground(new java.awt.Color(1, 1, 1));
        jLabel8.setText("fps");

        camFPS.setModel(new javax.swing.DefaultComboBoxModel(new String[]{"0.1", "0.2", "0.3", "0.4", "0.5", "1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23", "24"}));
        camFPS.setSelectedIndex(6);

        camPass.setText("12345");

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
                jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(jPanel3Layout.createSequentialGroup()
                                .addContainerGap()
                                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel3Layout.createSequentialGroup()
                                                .addComponent(jLabel11)
                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                                .addComponent(camTitle, javax.swing.GroupLayout.DEFAULT_SIZE, 188, Short.MAX_VALUE))
                                        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel3Layout.createSequentialGroup()
                                                .addComponent(camInvert)
                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 116, Short.MAX_VALUE)
                                                .addComponent(jLabel8)
                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                                .addComponent(camFPS, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel3Layout.createSequentialGroup()
                                                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                        .addComponent(jLabel9)
                                                        .addComponent(jLabel10))
                                                .addGap(18, 18, 18)
                                                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                                        .addComponent(camSize, 0, 168, Short.MAX_VALUE)
                                                        .addComponent(camPass, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 168, Short.MAX_VALUE)))
                                        .addGroup(jPanel3Layout.createSequentialGroup()
                                                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                                        .addComponent(jLabel12)
                                                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                                .addComponent(jLabel1)
                                                                .addComponent(jLabel7)))
                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                        .addComponent(camModel, javax.swing.GroupLayout.Alignment.TRAILING, 0, 202, Short.MAX_VALUE)
                                                        .addComponent(camUser, javax.swing.GroupLayout.DEFAULT_SIZE, 202, Short.MAX_VALUE)
                                                        .addComponent(camIP, javax.swing.GroupLayout.DEFAULT_SIZE, 202, Short.MAX_VALUE))))
                                .addContainerGap())
        );
        jPanel3Layout.setVerticalGroup(
                jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(jPanel3Layout.createSequentialGroup()
                                .addContainerGap()
                                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                        .addComponent(jLabel11)
                                        .addComponent(camTitle, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                        .addComponent(camModel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(jLabel12))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                        .addComponent(camIP, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(jLabel1))
                                .addGap(6, 6, 6)
                                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                        .addComponent(camUser, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(jLabel7))
                                .addGap(7, 7, 7)
                                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                        .addComponent(camPass, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(jLabel9))
                                .addGap(6, 6, 6)
                                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                        .addComponent(camSize, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(jLabel10))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                        .addComponent(camFPS, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(jLabel8)
                                        .addComponent(camInvert))
                                .addContainerGap(20, Short.MAX_VALUE))
        );

        jToolBar1.setBorder(null);
        jToolBar1.setFloatable(false);
        jToolBar1.setRollover(true);
        jToolBar1.setBorderPainted(false);
        jToolBar1.add(filler1);

        btnCamOK.setFont(new java.awt.Font("HGMaruGothicMPRO", 0, 14));
        btnCamOK.setIcon(new javax.swing.ImageIcon(getResource("images/disk.png"))); // NOI18N
        btnCamOK.setText("保存");
        btnCamOK.addActionListener(evt -> btnCamOKActionPerformed(evt));
        jToolBar1.add(btnCamOK);
        jToolBar1.add(filler2);

        javax.swing.GroupLayout ifCamLayout = new javax.swing.GroupLayout(ifCam.getContentPane());
        ifCam.getContentPane().setLayout(ifCamLayout);
        ifCamLayout.setHorizontalGroup(
                ifCamLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, ifCamLayout.createSequentialGroup()
                                .addContainerGap()
                                .addGroup(ifCamLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                        .addComponent(jPanel3, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                        .addComponent(jToolBar1, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 284, Short.MAX_VALUE))
                                .addContainerGap())
        );
        ifCamLayout.setVerticalGroup(
                ifCamLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, ifCamLayout.createSequentialGroup()
                                .addContainerGap()
                                .addComponent(jPanel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jToolBar1, javax.swing.GroupLayout.PREFERRED_SIZE, 41, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        ifCam.setBounds(630, 30, 310, 341);
        jDesktopPane.add(ifCam, javax.swing.JLayeredPane.DEFAULT_LAYER);

        ifCtrl.setClosable(true);
        ifCtrl.setDefaultCloseOperation(javax.swing.WindowConstants.HIDE_ON_CLOSE);
        ifCtrl.setIconifiable(true);
        ifCtrl.setTitle("カメラのコントロール");
        ifCtrl.setFocusable(false);

        jToolBar2.setFloatable(false);
        jToolBar2.setRollover(true);
        jToolBar2.add(filler4);

        jPanel2.setBackground(new java.awt.Color(221, 221, 221));

        btnCtrlZoomOut.setIcon(new javax.swing.ImageIcon(getResource("images/zoom_in.png"))); // NOI18N
        btnCtrlZoomOut.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                btnCtrlZoomOutClick(evt);
            }
        });

        btnCtrlDown.setIcon(new javax.swing.ImageIcon(getResource("images/arrow_down.png"))); // NOI18N
        btnCtrlDown.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                btnCtrlDownClick(evt);
            }
        });

        btnCtrlZoomIn.setIcon(new javax.swing.ImageIcon(getResource("images/zoom_out.png"))); // NOI18N
        btnCtrlZoomIn.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                btnCtrlZoomInClick(evt);
            }
        });

        btnCtrlRight.setIcon(new javax.swing.ImageIcon(getResource("images/arrow_right.png"))); // NOI18N
        btnCtrlRight.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                btnCtrlRightClick(evt);
            }
        });

        btnCtrlCenter.setIcon(new javax.swing.ImageIcon(getResource("images/arrow_in.png"))); // NOI18N
        btnCtrlCenter.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                btnCtrlCenterMouseClicked(evt);
            }
        });

        btnCtrlContrastUp.setIcon(new javax.swing.ImageIcon(getResource("images/contrast_increase.png"))); // NOI18N
        btnCtrlContrastUp.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                btnCtrlContrastUpClick(evt);
            }
        });

        btnCtrlLeft.setIcon(new javax.swing.ImageIcon(getResource("images/arrow_left.png"))); // NOI18N
        btnCtrlLeft.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                btnCtrlLeftClick(evt);
            }
        });

        btnCtrlContrastDown.setIcon(new javax.swing.ImageIcon(getResource("images/contrast_decrease.png"))); // NOI18N
        btnCtrlContrastDown.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                btnCtrlContrastDownClick(evt);
            }
        });

        btnCtrlUp.setIcon(new javax.swing.ImageIcon(getResource("images/arrow_up.png"))); // NOI18N
        btnCtrlUp.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                btnCtrlUpClick(evt);
            }
        });

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
                jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(jPanel2Layout.createSequentialGroup()
                                .addContainerGap()
                                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.CENTER)
                                        .addGroup(jPanel2Layout.createSequentialGroup()
                                                .addComponent(btnCtrlZoomOut)
                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                                .addComponent(btnCtrlDown)
                                                .addGap(6, 6, 6)
                                                .addComponent(btnCtrlZoomIn))
                                        .addGroup(jPanel2Layout.createSequentialGroup()
                                                .addComponent(btnCtrlLeft)
                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                                .addComponent(btnCtrlCenter)
                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                                .addComponent(btnCtrlRight))
                                        .addGroup(jPanel2Layout.createSequentialGroup()
                                                .addComponent(btnCtrlContrastUp)
                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                                .addComponent(btnCtrlUp)
                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                                .addComponent(btnCtrlContrastDown)))
                                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel2Layout.setVerticalGroup(
                jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(jPanel2Layout.createSequentialGroup()
                                .addContainerGap()
                                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addComponent(btnCtrlContrastUp)
                                        .addComponent(btnCtrlContrastDown)
                                        .addComponent(btnCtrlUp))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                        .addComponent(btnCtrlCenter)
                                        .addComponent(btnCtrlRight)
                                        .addComponent(btnCtrlLeft))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                        .addComponent(btnCtrlDown)
                                        .addComponent(btnCtrlZoomIn)
                                        .addComponent(btnCtrlZoomOut))
                                .addContainerGap(16, Short.MAX_VALUE))
        );

        jToolBar2.add(jPanel2);
        jToolBar2.add(filler3);

        javax.swing.GroupLayout ifCtrlLayout = new javax.swing.GroupLayout(ifCtrl.getContentPane());
        ifCtrl.getContentPane().setLayout(ifCtrlLayout);
        ifCtrlLayout.setHorizontalGroup(
                ifCtrlLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(jToolBar2, javax.swing.GroupLayout.DEFAULT_SIZE, 276, Short.MAX_VALUE)
        );
        ifCtrlLayout.setVerticalGroup(
                ifCtrlLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(jToolBar2, javax.swing.GroupLayout.PREFERRED_SIZE, 174, javax.swing.GroupLayout.PREFERRED_SIZE)
        );

        ifCtrl.setBounds(170, 130, 278, 210);
        jDesktopPane.add(ifCtrl, javax.swing.JLayeredPane.DEFAULT_LAYER);

        ifExport.setClosable(true);
        ifExport.setDefaultCloseOperation(javax.swing.WindowConstants.HIDE_ON_CLOSE);
        ifExport.setIconifiable(true);
        ifExport.setTitle("カメラ保存制定");

        jPanel1.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        jPanel1.setFocusCycleRoot(true);

        expQuality.setFont(new java.awt.Font("HGMaruGothicMPRO", 0, 14));
        expQuality.setModel(new javax.swing.DefaultComboBoxModel(new String[]{"低い", "普通", "高い"}));
        expQuality.setSelectedIndex(1);
        expQuality.setEnabled(false);

        expSize.setModel(new javax.swing.DefaultComboBoxModel(new String[]{"160x120", "200x150", "240x180", "280x210", "320x240"}));
        expSize.setSelectedIndex(4);
        expSize.setEnabled(false);

        expOnEvent.setFont(new java.awt.Font("HGMaruGothicMPRO", 0, 14));
        expOnEvent.setSelected(true);
        expOnEvent.setText("イベントのみで");
        expOnEvent.setEnabled(false);

        expBuffer.setFont(new java.awt.Font("HGMaruGothicMPRO", 0, 14));
        expBuffer.setModel(new javax.swing.DefaultComboBoxModel(new String[]{"なし", "1秒", "3秒", "5秒", "10秒", "15秒", "20秒", "30秒", "45秒", "60秒"}));
        expBuffer.setSelectedIndex(2);
        expBuffer.setEnabled(false);

        jLabel13.setFont(new java.awt.Font("HGMaruGothicMPRO", 0, 14));
        jLabel13.setText("事前事後のイベント：");

        jLabel14.setFont(new java.awt.Font("HGMaruGothicMPRO", 0, 14));
        jLabel14.setText("保存の品質");

        expPath.setEnabled(false);

        jLabel16.setFont(new java.awt.Font("HGMaruGothicMPRO", 0, 14));
        jLabel16.setText("保存パス：");

        btnChangePath.setFont(new java.awt.Font("HGMaruGothicMPRO", 0, 14));
        btnChangePath.setText("変更");
        btnChangePath.setEnabled(false);

        expResize.setFont(new java.awt.Font("HGMaruGothicMPRO", 0, 14));
        expResize.setSelected(true);
        expResize.setText("ビデオリサイズ");
        expResize.setEnabled(false);

        jLabel6.setFont(new java.awt.Font("HGMaruGothicMPRO", 0, 14));
        jLabel6.setText("レコーディングを保持：");

        jLabel15.setFont(new java.awt.Font("HGMaruGothicMPRO", 0, 14));
        jLabel15.setText("ログを保持:");

        jComboBox1.setFont(new java.awt.Font("HGMaruGothicMPRO", 0, 14));
        jComboBox1.setModel(new javax.swing.DefaultComboBoxModel(new String[]{"24時間", "2日間", "3日間", "4日間", "5日間", "一週間", "二週間", "三週間", "一ヶ月", "二ヶ月", "二ヶ月", "四ヶ月", "六ヶ月", "一年間"}));
        jComboBox1.setSelectedIndex(5);
        jComboBox1.setEnabled(false);

        jComboBox2.setFont(new java.awt.Font("HGMaruGothicMPRO", 0, 14));
        jComboBox2.setModel(new javax.swing.DefaultComboBoxModel(new String[]{"24時間", "2日間", "3日間", "4日間", "5日間", "一週間", "二週間", "三週間", "一ヶ月", "二ヶ月", "二ヶ月", "四ヶ月", "六ヶ月", "一年間"}));
        jComboBox2.setSelectedIndex(8);
        jComboBox2.setEnabled(false);

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
                jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(jPanel1Layout.createSequentialGroup()
                                .addContainerGap()
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addComponent(jSeparator2, javax.swing.GroupLayout.DEFAULT_SIZE, 354, Short.MAX_VALUE)
                                        .addComponent(expOnEvent)
                                        .addComponent(jSeparator3, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 354, Short.MAX_VALUE)
                                        .addGroup(jPanel1Layout.createSequentialGroup()
                                                .addComponent(jLabel13)
                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 146, Short.MAX_VALUE)
                                                .addComponent(expBuffer, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                        .addComponent(jLabel16)
                                        .addGroup(jPanel1Layout.createSequentialGroup()
                                                .addComponent(expPath, javax.swing.GroupLayout.PREFERRED_SIZE, 200, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                                .addComponent(btnChangePath))
                                        .addGroup(jPanel1Layout.createSequentialGroup()
                                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                        .addComponent(expResize)
                                                        .addComponent(jLabel14))
                                                .addGap(18, 18, 18)
                                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                                        .addComponent(expQuality, 0, 214, Short.MAX_VALUE)
                                                        .addComponent(expSize, javax.swing.GroupLayout.Alignment.LEADING, 0, 214, Short.MAX_VALUE)))
                                        .addComponent(jSeparator4, javax.swing.GroupLayout.DEFAULT_SIZE, 354, Short.MAX_VALUE)
                                        .addGroup(jPanel1Layout.createSequentialGroup()
                                                .addComponent(jLabel6)
                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 118, Short.MAX_VALUE)
                                                .addComponent(jComboBox1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                        .addGroup(jPanel1Layout.createSequentialGroup()
                                                .addComponent(jLabel15)
                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 199, Short.MAX_VALUE)
                                                .addComponent(jComboBox2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
                jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(jPanel1Layout.createSequentialGroup()
                                .addContainerGap()
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                        .addComponent(expQuality, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(jLabel14))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                        .addComponent(expSize, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(expResize))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jSeparator2, javax.swing.GroupLayout.PREFERRED_SIZE, 10, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(3, 3, 3)
                                .addComponent(expOnEvent)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                        .addComponent(jLabel13)
                                        .addComponent(expBuffer, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGap(16, 16, 16)
                                .addComponent(jSeparator3, javax.swing.GroupLayout.PREFERRED_SIZE, 10, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(2, 2, 2)
                                .addComponent(jLabel16)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                        .addComponent(expPath, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(btnChangePath))
                                .addGap(18, 18, 18)
                                .addComponent(jSeparator4, javax.swing.GroupLayout.PREFERRED_SIZE, 10, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(6, 6, 6)
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                        .addComponent(jLabel6)
                                        .addComponent(jComboBox1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                        .addComponent(jLabel15)
                                        .addComponent(jComboBox2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addContainerGap(25, Short.MAX_VALUE))
        );

        jToolBar3.setBorder(null);
        jToolBar3.setFloatable(false);
        jToolBar3.setRollover(true);
        jToolBar3.setBorderPainted(false);
        jToolBar3.add(filler5);

        btnExportOK.setFont(new java.awt.Font("HGMaruGothicMPRO", 0, 14));
        btnExportOK.setIcon(new javax.swing.ImageIcon(getResource("images/disk.png"))); // NOI18N
        btnExportOK.setText("保存");
        btnExportOK.setEnabled(false);
        btnExportOK.addActionListener(evt -> btnExportOKActionPerformed(evt));
        jToolBar3.add(btnExportOK);
        jToolBar3.add(filler6);

        javax.swing.GroupLayout ifExportLayout = new javax.swing.GroupLayout(ifExport.getContentPane());
        ifExport.getContentPane().setLayout(ifExportLayout);
        ifExportLayout.setHorizontalGroup(
                ifExportLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, ifExportLayout.createSequentialGroup()
                                .addContainerGap()
                                .addGroup(ifExportLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                        .addComponent(jPanel1, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                        .addComponent(jToolBar3, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 382, Short.MAX_VALUE))
                                .addContainerGap())
        );
        ifExportLayout.setVerticalGroup(
                ifExportLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, ifExportLayout.createSequentialGroup()
                                .addContainerGap()
                                .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jToolBar3, javax.swing.GroupLayout.PREFERRED_SIZE, 41, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        ifExport.setBounds(310, 30, 408, 427);
        jDesktopPane.add(ifExport, javax.swing.JLayeredPane.DEFAULT_LAYER);

        ifRules.setClosable(true);
        ifRules.setDefaultCloseOperation(javax.swing.WindowConstants.HIDE_ON_CLOSE);
        ifRules.setIconifiable(true);
        ifRules.setResizable(true);
        ifRules.setTitle("ルール管理");

        jPanel6.setPreferredSize(new java.awt.Dimension(200, 386));

        jLabel20.setFont(new java.awt.Font("HGMaruGothicMPRO", 0, 14));
        jLabel20.setIcon(new javax.swing.ImageIcon(getResource("images/attributes_display.png"))); // NOI18N
        jLabel20.setText("ルール:");

        rRuleList.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        rRuleList.setModel(new DefaultListModel());
        rRuleList.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                rRuleListMouseClicked(evt);
            }
        });

        rAddRule.setFont(new java.awt.Font("HGMaruGothicMPRO", 0, 12));
        rAddRule.setIcon(new javax.swing.ImageIcon(getResource("icons/add.png"))); // NOI18N
        rAddRule.setText("追加");
        rAddRule.addActionListener(evt -> rAddRuleActionPerformed(evt));

        rRmRule.setFont(new java.awt.Font("HGMaruGothicMPRO", 0, 12));
        rRmRule.setIcon(new javax.swing.ImageIcon(getResource("icons/del.png"))); // NOI18N
        rRmRule.setText("削除");
        rRmRule.addActionListener(evt -> rRmRuleActionPerformed(evt));

        javax.swing.GroupLayout jPanel6Layout = new javax.swing.GroupLayout(jPanel6);
        jPanel6.setLayout(jPanel6Layout);
        jPanel6Layout.setHorizontalGroup(
                jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(jPanel6Layout.createSequentialGroup()
                                .addComponent(jLabel20)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 29, Short.MAX_VALUE)
                                .addComponent(rRmRule)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(rAddRule))
                        .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addGroup(jPanel6Layout.createSequentialGroup()
                                        .addGap(1, 1, 1)
                                        .addComponent(rRuleList, javax.swing.GroupLayout.DEFAULT_SIZE, 227, Short.MAX_VALUE)))
        );
        jPanel6Layout.setVerticalGroup(
                jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(jPanel6Layout.createSequentialGroup()
                                .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                        .addComponent(jLabel20)
                                        .addComponent(rAddRule)
                                        .addComponent(rRmRule))
                                .addContainerGap(483, Short.MAX_VALUE))
                        .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addGroup(jPanel6Layout.createSequentialGroup()
                                        .addGap(36, 36, 36)
                                        .addComponent(rRuleList, javax.swing.GroupLayout.DEFAULT_SIZE, 479, Short.MAX_VALUE)))
        );

        jSplitPane1.setRightComponent(jPanel6);

        jPanel5.setPreferredSize(new java.awt.Dimension(200, 386));

        jLabel19.setFont(new java.awt.Font("HGMaruGothicMPRO", 0, 14));
        jLabel19.setIcon(new javax.swing.ImageIcon(getResource("images/draw_polyline.png"))); // NOI18N
        jLabel19.setText("エリア:");

        rAreaList.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        rAreaList.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
        rAreaList.setModel(new DefaultListModel());
        rAreaList.addListSelectionListener(new javax.swing.event.ListSelectionListener() {
            public void valueChanged(javax.swing.event.ListSelectionEvent evt) {
                rAreaListValueChanged(evt);
            }
        });

        javax.swing.GroupLayout jPanel5Layout = new javax.swing.GroupLayout(jPanel5);
        jPanel5.setLayout(jPanel5Layout);
        jPanel5Layout.setHorizontalGroup(
                jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(jPanel5Layout.createSequentialGroup()
                                .addComponent(jLabel19)
                                .addContainerGap(119, Short.MAX_VALUE))
                        .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addGroup(jPanel5Layout.createSequentialGroup()
                                        .addGap(1, 1, 1)
                                        .addComponent(rAreaList, javax.swing.GroupLayout.DEFAULT_SIZE, 199, Short.MAX_VALUE)))
        );
        jPanel5Layout.setVerticalGroup(
                jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(jPanel5Layout.createSequentialGroup()
                                .addComponent(jLabel19)
                                .addContainerGap(483, Short.MAX_VALUE))
                        .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addGroup(jPanel5Layout.createSequentialGroup()
                                        .addGap(36, 36, 36)
                                        .addComponent(rAreaList, javax.swing.GroupLayout.DEFAULT_SIZE, 479, Short.MAX_VALUE)))
        );

        jSplitPane1.setLeftComponent(jPanel5);

        javax.swing.GroupLayout ifRulesLayout = new javax.swing.GroupLayout(ifRules.getContentPane());
        ifRules.getContentPane().setLayout(ifRulesLayout);
        ifRulesLayout.setHorizontalGroup(
                ifRulesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(ifRulesLayout.createSequentialGroup()
                                .addContainerGap()
                                .addComponent(jSplitPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 434, Short.MAX_VALUE)
                                .addContainerGap())
        );
        ifRulesLayout.setVerticalGroup(
                ifRulesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(ifRulesLayout.createSequentialGroup()
                                .addContainerGap()
                                .addComponent(jSplitPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 515, Short.MAX_VALUE)
                                .addContainerGap())
        );

        ifRules.setBounds(430, 80, 460, 575);
        jDesktopPane.add(ifRules, javax.swing.JLayeredPane.DEFAULT_LAYER);

        ifRule.setClosable(true);
        ifRule.setDefaultCloseOperation(javax.swing.WindowConstants.HIDE_ON_CLOSE);
        ifRule.setTitle("ルール制定");
        ifRule.addComponentListener(new java.awt.event.ComponentAdapter() {
            public void componentHidden(java.awt.event.ComponentEvent evt) {
                ifRuleComponentHidden(evt);
            }
        });

        rRule.setFocusCycleRoot(true);
        rRule.setFont(new java.awt.Font("HGMaruGothicMPRO", 0, 14));
        rRule.setPreferredSize(new java.awt.Dimension(581, 415));

        rEventPanel.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "オプション", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("HGMaruGothicMPRO", 0, 14))); // NOI18N
        rEventPanel.setFocusCycleRoot(true);

        rFromM.setText("00");
        rFromM.setEnabled(false);

        jLabel21.setText("〜");

        jLabel22.setText(":");

        jLabel23.setText(":");

        jLabel24.setFont(new java.awt.Font("HGMaruGothicMPRO", 0, 14));
        jLabel24.setText("時間:");

        rToH.setText("23");
        rToH.setEnabled(false);

        rToM.setText("59");
        rToM.setEnabled(false);

        rMinU.setFont(new java.awt.Font("HGMaruGothicMPRO", 0, 14));
        rMinU.setModel(new javax.swing.DefaultComboBoxModel(new String[]{"秒", "分", "時"}));

        jLabel25.setFont(new java.awt.Font("HGMaruGothicMPRO", 0, 14));
        jLabel25.setText("経過時間:");

        rFromH.setText("00");
        rFromH.setEnabled(false);

        rMin.setText("0");

        rAlways.setFont(new java.awt.Font("HGMaruGothicMPRO", 0, 14));
        rAlways.setSelected(true);
        rAlways.setText("何時でも");
        rAlways.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                rAlwaysActionPerformed(evt);
            }
        });

        jLabel26.setText("Hrs");

        jLabel27.setText("EMail:");

        jLabel17.setFont(new java.awt.Font("HGMaruGothicMPRO", 0, 14));
        jLabel17.setText("実行：");

        rBtnBrowse.setFont(new java.awt.Font("HGMaruGothicMPRO", 0, 14));
        rBtnBrowse.setText("閲覧");
        rBtnBrowse.setEnabled(false);

        rExec.setEnabled(false);

        rUrl.setEnabled(false);

        jLabel29.setFont(new java.awt.Font("HGMaruGothicMPRO", 0, 14));
        jLabel29.setText("URL:");

        javax.swing.GroupLayout rAlertPanelLayout = new javax.swing.GroupLayout(rAlertPanel);
        rAlertPanel.setLayout(rAlertPanelLayout);
        rAlertPanelLayout.setHorizontalGroup(
                rAlertPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(rAlertPanelLayout.createSequentialGroup()
                                .addGroup(rAlertPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addComponent(jSeparator7, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 391, Short.MAX_VALUE)
                                        .addGroup(rAlertPanelLayout.createSequentialGroup()
                                                .addGroup(rAlertPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                        .addComponent(jLabel17)
                                                        .addComponent(jLabel29)
                                                        .addComponent(jLabel27))
                                                .addGap(39, 39, 39)
                                                .addGroup(rAlertPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                        .addComponent(rEmail, javax.swing.GroupLayout.DEFAULT_SIZE, 310, Short.MAX_VALUE)
                                                        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, rAlertPanelLayout.createSequentialGroup()
                                                                .addComponent(rExec, javax.swing.GroupLayout.DEFAULT_SIZE, 213, Short.MAX_VALUE)
                                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                                                .addComponent(rBtnBrowse, javax.swing.GroupLayout.PREFERRED_SIZE, 91, javax.swing.GroupLayout.PREFERRED_SIZE))
                                                        .addComponent(rUrl, javax.swing.GroupLayout.DEFAULT_SIZE, 310, Short.MAX_VALUE))))
                                .addContainerGap())
        );
        rAlertPanelLayout.setVerticalGroup(
                rAlertPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(rAlertPanelLayout.createSequentialGroup()
                                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(jSeparator7, javax.swing.GroupLayout.PREFERRED_SIZE, 6, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(rAlertPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                        .addComponent(rEmail, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(jLabel27))
                                .addGap(18, 18, 18)
                                .addGroup(rAlertPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                        .addComponent(jLabel17)
                                        .addComponent(rExec, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(rBtnBrowse))
                                .addGap(18, 18, 18)
                                .addGroup(rAlertPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                        .addComponent(rUrl, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(jLabel29))
                                .addGap(6, 6, 6))
        );

        jLabel28.setFont(new java.awt.Font("HGMaruGothicMPRO", 0, 14));
        jLabel28.setText("音：");

        rSoundAlert.setFont(new java.awt.Font("HGMaruGothicMPRO", 0, 14));
        rSoundAlert.setModel(new javax.swing.DefaultComboBoxModel(new String[]{"再生しないで", "アラーム1", "アラーム2", "アラーム3", "アラーム4", "アラーム5"}));
        rSoundAlert.setEnabled(false);
        rSoundAlert.addActionListener(evt -> rSoundAlertActionPerformed(evt));

        rPlaySound.setIcon(new javax.swing.ImageIcon(getResource("icons/sound.png"))); // NOI18N
        rPlaySound.setEnabled(false);
        rPlaySound.addActionListener(evt -> rPlaySoundActionPerformed(evt));

        rAddSound.setIcon(new javax.swing.ImageIcon(getResource("icons/sound_add.png"))); // NOI18N
        rAddSound.setEnabled(false);

        javax.swing.GroupLayout rEventPanelLayout = new javax.swing.GroupLayout(rEventPanel);
        rEventPanel.setLayout(rEventPanelLayout);
        rEventPanelLayout.setHorizontalGroup(
                rEventPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(rEventPanelLayout.createSequentialGroup()
                                .addContainerGap()
                                .addGroup(rEventPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addGroup(rEventPanelLayout.createSequentialGroup()
                                                .addGroup(rEventPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                        .addComponent(rAlertPanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                                        .addGroup(rEventPanelLayout.createSequentialGroup()
                                                                .addGap(71, 71, 71)
                                                                .addComponent(rMin, javax.swing.GroupLayout.PREFERRED_SIZE, 39, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                                                .addComponent(rMinU, javax.swing.GroupLayout.PREFERRED_SIZE, 66, javax.swing.GroupLayout.PREFERRED_SIZE))
                                                        .addComponent(jLabel25)
                                                        .addComponent(jSeparator6, javax.swing.GroupLayout.DEFAULT_SIZE, 403, Short.MAX_VALUE)
                                                        .addComponent(rAlways)
                                                        .addGroup(rEventPanelLayout.createSequentialGroup()
                                                                .addComponent(jLabel24)
                                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                                                .addComponent(rFromH, javax.swing.GroupLayout.PREFERRED_SIZE, 29, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                                .addGap(0, 0, 0)
                                                                .addComponent(jLabel22)
                                                                .addGap(0, 0, 0)
                                                                .addComponent(rFromM, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                                .addGap(1, 1, 1)
                                                                .addComponent(jLabel21)
                                                                .addGap(1, 1, 1)
                                                                .addComponent(rToH, javax.swing.GroupLayout.PREFERRED_SIZE, 29, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                                .addGap(0, 0, 0)
                                                                .addComponent(jLabel23)
                                                                .addGap(0, 0, 0)
                                                                .addComponent(rToM, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                                .addGap(5, 5, 5)
                                                                .addComponent(jLabel26)))
                                                .addContainerGap())
                                        .addGroup(rEventPanelLayout.createSequentialGroup()
                                                .addComponent(jLabel28, javax.swing.GroupLayout.DEFAULT_SIZE, 71, Short.MAX_VALUE)
                                                .addGap(43, 43, 43)
                                                .addComponent(rAddSound)
                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                                .addComponent(rSoundAlert, 0, 209, Short.MAX_VALUE)
                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                                .addComponent(rPlaySound)
                                                .addGap(24, 24, 24))))
        );
        rEventPanelLayout.setVerticalGroup(
                rEventPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(rEventPanelLayout.createSequentialGroup()
                                .addContainerGap()
                                .addGroup(rEventPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                        .addComponent(jLabel25)
                                        .addComponent(rMin, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(rMinU, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGap(18, 18, 18)
                                .addComponent(jSeparator6, javax.swing.GroupLayout.PREFERRED_SIZE, 10, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addComponent(rAlways)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(rEventPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                        .addComponent(jLabel24)
                                        .addComponent(rFromH, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(jLabel22)
                                        .addComponent(rFromM, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(jLabel21)
                                        .addComponent(rToH, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(jLabel23)
                                        .addComponent(rToM, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(jLabel26))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(rAlertPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addGroup(rEventPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                        .addGroup(rEventPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                                .addComponent(rSoundAlert, javax.swing.GroupLayout.PREFERRED_SIZE, 28, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addComponent(jLabel28))
                                        .addComponent(rPlaySound)
                                        .addComponent(rAddSound))
                                .addContainerGap(18, Short.MAX_VALUE))
        );

        jToolBar4.setBorder(null);
        jToolBar4.setFloatable(false);
        jToolBar4.setRollover(true);
        jToolBar4.setBorderPainted(false);
        jToolBar4.add(filler7);

        btnRuleOK.setFont(new java.awt.Font("HGMaruGothicMPRO", 0, 14));
        btnRuleOK.setIcon(new javax.swing.ImageIcon(getResource("images/disk.png"))); // NOI18N
        btnRuleOK.setText("保存");
        btnRuleOK.addActionListener(evt -> btnRuleOKActionPerformed(evt));
        jToolBar4.add(btnRuleOK);
        jToolBar4.add(filler8);

        jToolBar5.setFloatable(false);
        jToolBar5.setRollover(true);
        jToolBar5.add(filler10);

        gbRule.add(btnNoLog);
        btnNoLog.setFont(new java.awt.Font("HGMaruGothicMPRO", 0, 14));
        btnNoLog.setIcon(new javax.swing.ImageIcon(getResource("images/cross.png"))); // NOI18N
        btnNoLog.setText("ログしないで");
        btnNoLog.setFocusable(false);
        btnNoLog.setHorizontalTextPosition(javax.swing.SwingConstants.RIGHT);
        btnNoLog.addActionListener(evt -> btnNoLogActionPerformed(evt));
        jToolBar5.add(btnNoLog);

        gbRule.add(btnLogOnly);
        btnLogOnly.setFont(new java.awt.Font("HGMaruGothicMPRO", 0, 14));
        btnLogOnly.setIcon(new javax.swing.ImageIcon(getResource("images/report_edit.png"))); // NOI18N
        btnLogOnly.setSelected(true);
        btnLogOnly.setText("ログにして");
        btnLogOnly.setFocusable(false);
        btnLogOnly.setHorizontalTextPosition(javax.swing.SwingConstants.RIGHT);
        btnLogOnly.addActionListener(evt -> btnLogOnlyActionPerformed(evt));
        jToolBar5.add(btnLogOnly);

        gbRule.add(btnLogAlert);
        btnLogAlert.setFont(new java.awt.Font("HGMaruGothicMPRO", 0, 14));
        btnLogAlert.setIcon(new javax.swing.ImageIcon(getResource("images/alarm_bell.png"))); // NOI18N
        btnLogAlert.setText("＋アラート");
        btnLogAlert.setFocusable(false);
        btnLogAlert.setHorizontalTextPosition(javax.swing.SwingConstants.RIGHT);
        btnLogAlert.addActionListener(evt -> btnLogAlertActionPerformed(evt));
        jToolBar5.add(btnLogAlert);
        jToolBar5.add(filler9);

        jLabel2.setFont(new java.awt.Font("HGMaruGothicMPRO", 0, 14));
        jLabel2.setText("ルール名：");

        rRuleName.setFont(new java.awt.Font("HGMaruGothicMPRO", 0, 14));
        rRuleName.setText("ルール1");
        //rRuleName.setNextFocusableComponent(rMin); //FIXME: deprecated

        javax.swing.GroupLayout rRuleLayout = new javax.swing.GroupLayout(rRule);
        rRule.setLayout(rRuleLayout);
        rRuleLayout.setHorizontalGroup(
                rRuleLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, rRuleLayout.createSequentialGroup()
                                .addContainerGap()
                                .addGroup(rRuleLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                        .addComponent(rEventPanel, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                        .addGroup(javax.swing.GroupLayout.Alignment.LEADING, rRuleLayout.createSequentialGroup()
                                                .addComponent(jLabel2)
                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                                .addComponent(rRuleName, javax.swing.GroupLayout.DEFAULT_SIZE, 357, Short.MAX_VALUE)))
                                .addContainerGap())
                        .addComponent(jToolBar4, javax.swing.GroupLayout.DEFAULT_SIZE, 463, Short.MAX_VALUE)
                        .addComponent(jToolBar5, javax.swing.GroupLayout.DEFAULT_SIZE, 463, Short.MAX_VALUE)
        );
        rRuleLayout.setVerticalGroup(
                rRuleLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(rRuleLayout.createSequentialGroup()
                                .addComponent(jToolBar5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(rRuleLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                        .addComponent(jLabel2)
                                        .addComponent(rRuleName, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(rEventPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jToolBar4, javax.swing.GroupLayout.DEFAULT_SIZE, 58, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout ifRuleLayout = new javax.swing.GroupLayout(ifRule.getContentPane());
        ifRule.getContentPane().setLayout(ifRuleLayout);
        ifRuleLayout.setHorizontalGroup(
                ifRuleLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 463, Short.MAX_VALUE)
                        .addGroup(ifRuleLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addComponent(rRule, javax.swing.GroupLayout.DEFAULT_SIZE, 463, Short.MAX_VALUE))
        );
        ifRuleLayout.setVerticalGroup(
                ifRuleLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 523, Short.MAX_VALUE)
                        .addGroup(ifRuleLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addComponent(rRule, javax.swing.GroupLayout.DEFAULT_SIZE, 523, Short.MAX_VALUE))
        );

        ifRule.setBounds(50, 70, 465, 559);
        jDesktopPane.add(ifRule, javax.swing.JLayeredPane.DEFAULT_LAYER);

        ifAbout.setClosable(true);
        ifAbout.setDefaultCloseOperation(javax.swing.WindowConstants.HIDE_ON_CLOSE);
        ifAbout.setTitle("について");

        logo.setFont(new java.awt.Font("DejaVu Sans", 3, 13));
        logo.setForeground(new java.awt.Color(91, 91, 91));
        logo.setIcon(new javax.swing.ImageIcon(getResource("etc/logo.png"))); // NOI18N
        logo.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        logo.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                logoMouseClicked(evt);
            }
        });

        jLabel3.setBackground(new java.awt.Color(1, 1, 1));
        jLabel3.setForeground(new java.awt.Color(254, 254, 254));
        jLabel3.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel3.setText("@CopyRight " + LocalDate.now().format(DateTimeFormatter.ofPattern("YYYY")));
        jLabel3.setOpaque(true);

        jLabel4.setFont(new java.awt.Font("HGMaruGothicMPRO", 0, 10));
        jLabel4.setText("メインデベロッパー：　レペ");

        jLabel5.setIcon(new javax.swing.ImageIcon(getResource("etc/banner.jpg"))); // NOI18N

        jLabel18.setFont(new java.awt.Font("HGMaruGothicMPRO", 0, 10));
        jLabel18.setText("バージョン：");

        version.setFont(new java.awt.Font("Arial", 1, 11));
        version.setText("0.5");

        javax.swing.GroupLayout ifAboutLayout = new javax.swing.GroupLayout(ifAbout.getContentPane());
        ifAbout.getContentPane().setLayout(ifAboutLayout);
        ifAboutLayout.setHorizontalGroup(
                ifAboutLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(jLabel3, javax.swing.GroupLayout.DEFAULT_SIZE, 280, Short.MAX_VALUE)
                        .addComponent(jLabel5, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGroup(ifAboutLayout.createSequentialGroup()
                                .addContainerGap()
                                .addGroup(ifAboutLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addComponent(jLabel4)
                                        .addGroup(ifAboutLayout.createSequentialGroup()
                                                .addComponent(jLabel18)
                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                                .addComponent(version)))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 30, Short.MAX_VALUE)
                                .addComponent(logo)
                                .addContainerGap())
        );
        ifAboutLayout.setVerticalGroup(
                ifAboutLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(ifAboutLayout.createSequentialGroup()
                                .addComponent(jLabel5)
                                .addGap(0, 0, 0)
                                .addComponent(jLabel3)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(ifAboutLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                        .addGroup(ifAboutLayout.createSequentialGroup()
                                                .addGroup(ifAboutLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                                        .addComponent(jLabel18)
                                                        .addComponent(version))
                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                                .addComponent(jLabel4))
                                        .addComponent(logo, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        ifAbout.setBounds(300, 130, 282, 160);
        jDesktopPane.add(ifAbout, javax.swing.JLayeredPane.DEFAULT_LAYER);

        ifPlayer.setClosable(true);
        ifPlayer.setDefaultCloseOperation(javax.swing.WindowConstants.HIDE_ON_CLOSE);
        ifPlayer.setIconifiable(true);
        ifPlayer.setMaximizable(true);
        ifPlayer.setResizable(true);
        ifPlayer.setTitle("プレイヤー");
        vPlayer.counter = vPlayerCounter;
        ifPlayer.addInternalFrameListener(new javax.swing.event.InternalFrameListener() {
            public void internalFrameActivated(javax.swing.event.InternalFrameEvent evt) {
            }

            public void internalFrameClosed(javax.swing.event.InternalFrameEvent evt) {
            }

            public void internalFrameClosing(javax.swing.event.InternalFrameEvent evt) {
                ifPlayerInternalFrameClosing(evt);
            }

            public void internalFrameDeactivated(javax.swing.event.InternalFrameEvent evt) {
            }

            public void internalFrameDeiconified(javax.swing.event.InternalFrameEvent evt) {
            }

            public void internalFrameIconified(javax.swing.event.InternalFrameEvent evt) {
            }

            public void internalFrameOpened(javax.swing.event.InternalFrameEvent evt) {
            }
        });

        vPlayer.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        javax.swing.GroupLayout vPlayerLayout = new javax.swing.GroupLayout(vPlayer);
        vPlayer.setLayout(vPlayerLayout);
        vPlayerLayout.setHorizontalGroup(
                vPlayerLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 410, Short.MAX_VALUE)
        );
        vPlayerLayout.setVerticalGroup(
                vPlayerLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 279, Short.MAX_VALUE)
        );

        jToolBar6.setFloatable(false);
        jToolBar6.setRollover(true);
        jToolBar6.add(filler11);

        btnBegin.setIcon(new javax.swing.ImageIcon(getResource("images/control_start.png"))); // NOI18N
        btnBegin.setText("Begin");
        btnBegin.setEnabled(false);
        btnBegin.setFocusable(false);
        btnBegin.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btnBegin.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        jToolBar6.add(btnBegin);

        btnRew.setIcon(new javax.swing.ImageIcon(getResource("images/control_rewind.png"))); // NOI18N
        btnRew.setText("Slow");
        btnRew.setFocusable(false);
        btnRew.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btnRew.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        btnRew.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                btnRewMouseClicked(evt);
            }
        });
        jToolBar6.add(btnRew);

        btnPause.setIcon(new javax.swing.ImageIcon(getResource("images/control_pause_blue.png"))); // NOI18N
        btnPause.setText("Pause");
        btnPause.setFocusable(false);
        btnPause.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btnPause.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        btnPause.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                btnPauseMouseClicked(evt);
            }
        });
        jToolBar6.add(btnPause);

        btnPlay.setIcon(new javax.swing.ImageIcon(getResource("images/control_play_blue.png"))); // NOI18N
        btnPlay.setText("Play");
        btnPlay.setFocusable(false);
        btnPlay.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btnPlay.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        btnPlay.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                btnPlayMouseClicked(evt);
            }
        });
        jToolBar6.add(btnPlay);

        btnFwd.setIcon(new javax.swing.ImageIcon(getResource("images/control_fastforward.png"))); // NOI18N
        btnFwd.setText("Fast");
        btnFwd.setFocusable(false);
        btnFwd.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btnFwd.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        btnFwd.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                btnFwdMouseClicked(evt);
            }
        });
        jToolBar6.add(btnFwd);

        btnEnd.setIcon(new javax.swing.ImageIcon(getResource("images/control_end.png"))); // NOI18N
        btnEnd.setText("End");
        btnEnd.setEnabled(false);
        btnEnd.setFocusable(false);
        btnEnd.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btnEnd.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        jToolBar6.add(btnEnd);
        jToolBar6.add(filler12);

        vPlayerCounter.setBackground(new java.awt.Color(1, 1, 1));
        vPlayerCounter.setForeground(new java.awt.Color(254, 254, 254));
        vPlayerCounter.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        vPlayerCounter.setText("0000-00-00 00:00:00");
        vPlayerCounter.setOpaque(true);

        javax.swing.GroupLayout ifPlayerLayout = new javax.swing.GroupLayout(ifPlayer.getContentPane());
        ifPlayer.getContentPane().setLayout(ifPlayerLayout);
        ifPlayerLayout.setHorizontalGroup(
                ifPlayerLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(vPlayerCounter, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 438, Short.MAX_VALUE)
                        .addGroup(ifPlayerLayout.createSequentialGroup()
                                .addContainerGap()
                                .addGroup(ifPlayerLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addComponent(vPlayer, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                        .addComponent(jToolBar6, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 414, Short.MAX_VALUE))
                                .addContainerGap())
        );
        ifPlayerLayout.setVerticalGroup(
                ifPlayerLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, ifPlayerLayout.createSequentialGroup()
                                .addContainerGap()
                                .addComponent(vPlayer, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jToolBar6, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(vPlayerCounter))
        );

        ifPlayer.setBounds(360, 80, 440, 427);
        jDesktopPane.add(ifPlayer, javax.swing.JLayeredPane.DEFAULT_LAYER);

        ifLog.setClosable(true);
        ifLog.setDefaultCloseOperation(javax.swing.WindowConstants.HIDE_ON_CLOSE);
        ifLog.setIconifiable(true);
        ifLog.setMaximizable(true);
        ifLog.setResizable(true);
        ifLog.setTitle("ログビューアー");

        tLog.setFont(new java.awt.Font("HGMaruGothicMPRO", 0, 14));
        tLog.setModel(new javax.swing.table.DefaultTableModel(
                new Object[][]{

                },
                new String[]{
                        "始時間", "終時間", "経過秒", "エリア", "ルール"
                }
        ) {
            Class[] types = new Class[]{
                    java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.String.class
            };
            boolean[] canEdit = new boolean[]{
                    false, false, false, false, false
            };

            public Class getColumnClass(int columnIndex) {
                return types[columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit[columnIndex];
            }
        });
        jScrollPane1.setViewportView(tLog);

        javax.swing.GroupLayout ifLogLayout = new javax.swing.GroupLayout(ifLog.getContentPane());
        ifLog.getContentPane().setLayout(ifLogLayout);
        ifLogLayout.setHorizontalGroup(
                ifLogLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 607, Short.MAX_VALUE)
        );
        ifLogLayout.setVerticalGroup(
                ifLogLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(jScrollPane1, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 482, Short.MAX_VALUE)
        );

        ifLog.setBounds(70, 80, 609, 518);
        jDesktopPane.add(ifLog, javax.swing.JLayeredPane.DEFAULT_LAYER);

        mnCam.setIcon(new javax.swing.ImageIcon(getResource("images/webcam.png"))); // NOI18N
        mnCam.setText("カメラ");
        mnCam.setFont(new java.awt.Font("HGMaruGothicMPRO", 0, 14));

        mnCamAdd.setFont(new java.awt.Font("HGMaruGothicMPRO", 0, 14));
        mnCamAdd.setIcon(new javax.swing.ImageIcon(getResource("images/webcam_add.png"))); // NOI18N
        mnCamAdd.setText("追加 ...");
        mnCamAdd.addActionListener(evt -> mnCamAddActionPerformed(evt));
        mnCam.add(mnCamAdd);

        mnCamRm.setFont(new java.awt.Font("HGMaruGothicMPRO", 0, 14));
        mnCamRm.setIcon(new javax.swing.ImageIcon(getResource("images/webcam_delete.png"))); // NOI18N
        mnCamRm.setText("削除");
        mnCamRm.setEnabled(false);
        mnCamRm.addActionListener(evt -> mnCamRmActionPerformed(evt));
        mnCam.add(mnCamRm);
        mnCam.add(jSeparator1);

        jMenuBar1.add(mnCam);

        mnArea.setIcon(new javax.swing.ImageIcon(getResource("images/draw_polyline.png"))); // NOI18N
        mnArea.setText("エリア");
        mnArea.setEnabled(false);
        mnArea.setFont(new java.awt.Font("HGMaruGothicMPRO", 0, 14));

        mnAreaAdd.setFont(new java.awt.Font("HGMaruGothicMPRO", 0, 14));
        mnAreaAdd.setIcon(new javax.swing.ImageIcon(getResource("images/add.png"))); // NOI18N
        mnAreaAdd.setText("追加");
        mnAreaAdd.addActionListener(evt -> mnAreaAddActionPerformed(evt));
        mnArea.add(mnAreaAdd);

        mnAreaRm.setFont(new java.awt.Font("HGMaruGothicMPRO", 0, 14));
        mnAreaRm.setIcon(new javax.swing.ImageIcon(getResource("images/delete.png"))); // NOI18N
        mnAreaRm.setText("削除");
        mnAreaRm.addActionListener(evt -> mnAreaRmActionPerformed(evt));
        mnArea.add(mnAreaRm);

        mnAreaView.setFont(new java.awt.Font("HGMaruGothicMPRO", 0, 14));
        mnAreaView.setIcon(new javax.swing.ImageIcon(getResource("images/eye.png"))); // NOI18N
        mnAreaView.setText("視度");
        mnAreaView.addActionListener(evt -> mnAreaViewActionPerformed(evt));
        mnArea.add(mnAreaView);

        jMenuBar1.add(mnArea);

        mnTools.setIcon(new javax.swing.ImageIcon(getResource("images/widgets.png"))); // NOI18N
        mnTools.setText("ツール");
        mnTools.setEnabled(false);
        mnTools.setFont(new java.awt.Font("HGMaruGothicMPRO", 0, 14));

        mnRule.setFont(new java.awt.Font("HGMaruGothicMPRO", 0, 14));
        mnRule.setIcon(new javax.swing.ImageIcon(getResource("images/attributes_display.png"))); // NOI18N
        mnRule.setText("ルール");
        mnRule.addActionListener(evt -> mnRuleActionPerformed(evt));
        mnTools.add(mnRule);

        mnLog.setFont(new java.awt.Font("HGMaruGothicMPRO", 0, 14));
        mnLog.setIcon(new javax.swing.ImageIcon(getResource("images/report_magnify.png"))); // NOI18N
        mnLog.setText("ログ");
        mnLog.addActionListener(evt -> mnLogActionPerformed(evt));
        mnTools.add(mnLog);

        mnReset.setFont(new java.awt.Font("HGMaruGothicMPRO", 0, 14));
        mnReset.setIcon(new javax.swing.ImageIcon(getResource("images/update.png"))); // NOI18N
        mnReset.setText("リセット");
        mnReset.addActionListener(evt -> mnResetActionPerformed(evt));
        mnTools.add(mnReset);

        mnCtrl.setFont(new java.awt.Font("HGMaruGothicMPRO", 0, 14));
        mnCtrl.setIcon(new javax.swing.ImageIcon(getResource("images/joystick.png"))); // NOI18N
        mnCtrl.setText("コントロール");
        mnCtrl.addActionListener(evt -> mnCtrlActionPerformed(evt));
        mnTools.add(mnCtrl);

        mnOpts.setFont(new java.awt.Font("HGMaruGothicMPRO", 0, 14));
        mnOpts.setIcon(new javax.swing.ImageIcon(getResource("images/setting_tools.png"))); // NOI18N
        mnOpts.setText("保存制定");
        mnOpts.addActionListener(evt -> mnOptsActionPerformed(evt));
        mnTools.add(mnOpts);

        jMenuBar1.add(mnTools);

        mnHelp.setIcon(new javax.swing.ImageIcon(getResource("images/help.png"))); // NOI18N
        mnHelp.setText("ヘルプ");
        mnHelp.setFont(new java.awt.Font("HGMaruGothicMPRO", 0, 14));

        mnAbout.setFont(new java.awt.Font("HGMaruGothicMPRO", 0, 14));
        mnAbout.setIcon(new javax.swing.ImageIcon(getResource("images/attribution.png"))); // NOI18N
        mnAbout.setText("について");
        mnAbout.addActionListener(evt -> mnAboutActionPerformed(evt));
        mnHelp.add(mnAbout);

        mnUpd.setFont(new java.awt.Font("HGMaruGothicMPRO", 0, 14));
        mnUpd.setIcon(new javax.swing.ImageIcon(getResource("images/global_telecom.png"))); // NOI18N
        mnUpd.setText("更新");
        mnUpd.addActionListener(evt -> mnUpdActionPerformed(evt));
        mnHelp.add(mnUpd);

        mnDoc.setFont(new java.awt.Font("HGMaruGothicMPRO", 0, 14));
        mnDoc.setIcon(new javax.swing.ImageIcon(getResource("images/book.png"))); // NOI18N
        mnDoc.setText("オンライン文書");
        mnDoc.addActionListener(evt -> mnDocActionPerformed(evt));
        mnHelp.add(mnDoc);

        jMenuBar1.add(mnHelp);

        setJMenuBar(jMenuBar1);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
                layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(jDesktopPane, javax.swing.GroupLayout.DEFAULT_SIZE, 976, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
                layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(jDesktopPane, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 634, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnCtrlZoomInClick(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnCtrlZoomInClick
        try {
            wURL.get(W.getConfig().camera.url.toExternalForm() + W.getConfig().in);
        } catch (Exception e) {
            e.printStackTrace();
        }
        W.getConfig().watcher.reset();
    }//GEN-LAST:event_btnCtrlZoomInClick

    private void btnCtrlZoomOutClick(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnCtrlZoomOutClick
        try {
            wURL.get(W.getConfig().camera.url.toExternalForm() + W.getConfig().out);
        } catch (Exception e) {
            e.printStackTrace();
        }
        W.getConfig().watcher.reset();
    }//GEN-LAST:event_btnCtrlZoomOutClick

    private void btnCtrlUpClick(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnCtrlUpClick
        try {
            if (W.getConfig().camera.invert)
                wURL.get(W.getConfig().camera.url.toExternalForm() + W.getConfig().down);
            else
                wURL.get(W.getConfig().camera.url.toExternalForm() + W.getConfig().up);
        } catch (Exception e) {
            e.printStackTrace();
        }
        W.getConfig().watcher.reset();

    }//GEN-LAST:event_btnCtrlUpClick

    private void btnCtrlDownClick(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnCtrlDownClick
        try {
            if (W.getConfig().camera.invert)
                wURL.get(W.getConfig().camera.url.toExternalForm() + W.getConfig().up);
            else
                wURL.get(W.getConfig().camera.url.toExternalForm() + W.getConfig().down);
        } catch (Exception e) {
            e.printStackTrace();
        }
        W.getConfig().watcher.reset();
    }//GEN-LAST:event_btnCtrlDownClick

    private void btnCtrlLeftClick(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnCtrlLeftClick
        try {
            if (W.getConfig().camera.invert)
                wURL.get(W.getConfig().camera.url.toExternalForm() + W.getConfig().right);
            else
                wURL.get(W.getConfig().camera.url.toExternalForm() + W.getConfig().left);
        } catch (Exception e) {
            e.printStackTrace();
        }
        W.getConfig().watcher.reset();
    }//GEN-LAST:event_btnCtrlLeftClick

    private void btnCtrlRightClick(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnCtrlRightClick
        try {
            if (W.getConfig().camera.invert)
                wURL.get(W.getConfig().camera.url.toExternalForm() + W.getConfig().left);
            else
                wURL.get(W.getConfig().camera.url.toExternalForm() + W.getConfig().right);
        } catch (Exception e) {
            e.printStackTrace();
        }
        W.getConfig().watcher.reset();

    }//GEN-LAST:event_btnCtrlRightClick

    private void btnCtrlContrastUpClick(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnCtrlContrastUpClick
        try {
            wURL.get(W.getConfig().camera.url.toExternalForm() + W.getConfig().contrastp);
        } catch (Exception e) {
            e.printStackTrace();
        }
        W.getConfig().watcher.reset();

    }//GEN-LAST:event_btnCtrlContrastUpClick

    private void btnCtrlCenterMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnCtrlCenterMouseClicked
        try {
            wURL.get(W.getConfig().camera.url.toExternalForm() + W.getConfig().home);
        } catch (Exception e) {
            e.printStackTrace();
        }
        W.getConfig().watcher.reset();
    }//GEN-LAST:event_btnCtrlCenterMouseClicked

    private void btnCtrlContrastDownClick(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnCtrlContrastDownClick
        try {
            wURL.get(W.getConfig().camera.url.toExternalForm() + W.getConfig().contrastm);
        } catch (Exception e) {
            e.printStackTrace();
        }
        W.getConfig().watcher.reset();

    }//GEN-LAST:event_btnCtrlContrastDownClick

    private void mnCamAddActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_mnCamAddActionPerformed
        centerFrame(ifCam);
        List<String> cams = null;
        cams = wDB.getCamModels();
        camModel.removeAllItems();
        for (String cam : cams) {
            camModel.addItem(cam);
        }
        ifCam.setVisible(true);
    }//GEN-LAST:event_mnCamAddActionPerformed

    private void mnCamRmActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_mnCamRmActionPerformed
        int answer = JOptionPane.showInternalConfirmDialog(jDesktopPane, "本当に削除していいですか？", "Watcher", JOptionPane.OK_CANCEL_OPTION);
        if (answer == 0) {
            int cid = W.selCam;
            try {
                FileUtils.deleteDirectory(new File(W.getCamera().exp_path + File.separator + cid));
            } catch (Exception e) {
                e.printStackTrace();
            }
            mnCam.remove(W.getConfig().vmenu);
            if (W.getConfig().viframe != null) W.getConfig().viframe.dispose(); //FIXME: viframe should be there
            wDB.destroy(cid);
        }
    }//GEN-LAST:event_mnCamRmActionPerformed

    private void btnCamOKActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCamOKActionPerformed
        Camera cam = new Camera();
        try {
            cam.url = new URL("http://" + camUser.getText() + ":" + String.valueOf(camPass.getPassword()) + "@" + camIP.getText() + "/");
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
        cam.size = camSize.getSelectedItem().toString();
        cam.fps = (int) (Double.parseDouble(camFPS.getSelectedItem().toString()) * 10);
        cam.invert = camInvert.isSelected();

        int nextID = W.selCam = wDB.create(camTitle.getText(), wDB.getModelId(camModel.getSelectedItem().toString()), cam);
        addCameraInMenu(camTitle.getText(), nextID, true);
        addIFLog(W.getConfig(nextID));

        ifCam.setVisible(false);
    }//GEN-LAST:event_btnCamOKActionPerformed

    private void camModelActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_camModelActionPerformed
        if ("comboBoxChanged".equals(evt.getActionCommand())) {
            if (camModel.getItemCount() > 0) {
                CameraConfig cc = wDB.getModelConfig(camModel.getSelectedItem().toString());
                if (cc != null) {
                    camUser.setText(cc.def_user);
                    camPass.setText(cc.def_pass);
                    camSize.removeAllItems();
                    for (String size : cc.resolutions.split(",")) {
                        camSize.addItem(size);
                    }
                }
            }
        }
    }//GEN-LAST:event_camModelActionPerformed

    private void mnResetActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_mnResetActionPerformed
        W.getConfig().watcher.reset();
    }//GEN-LAST:event_mnResetActionPerformed

    private void mnCtrlActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_mnCtrlActionPerformed
        centerFrame(ifCtrl);
        ifCtrl.setVisible(true);
    }//GEN-LAST:event_mnCtrlActionPerformed

    private void mnAboutActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_mnAboutActionPerformed
        centerFrame(ifAbout);
        version.setText(W.props.getProperty("app.version"));
        ifAbout.setVisible(true);
    }//GEN-LAST:event_mnAboutActionPerformed

    private void mnAreaAddActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_mnAreaAddActionPerformed
        VPanel video = W.getConfig().video;
        video.newname = (String) JOptionPane.showInputDialog(this, "エリア名を入力してください：");
        video.edit(true);

    }//GEN-LAST:event_mnAreaAddActionPerformed

    private void mnRuleActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_mnRuleActionPerformed
        centerFrame(ifRules);
        refreshAreaList();
        refreshRuleList();
        ifRules.show();
    }//GEN-LAST:event_mnRuleActionPerformed

    private void mnAreaRmActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_mnAreaRmActionPerformed
        W.getConfig().video.delete();
        wDB.save(W.selCam);
    }//GEN-LAST:event_mnAreaRmActionPerformed

    private void mnAreaViewActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_mnAreaViewActionPerformed
        W.getConfig().video.showAreas();
    }//GEN-LAST:event_mnAreaViewActionPerformed

    private void mnLogActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_mnLogActionPerformed
        CameraConfig cc = W.getConfig();
        centerFrame(cc.logger);
        cc.logger.setVisible(true);
    }//GEN-LAST:event_mnLogActionPerformed

    private void mnOptsActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_mnOptsActionPerformed
        ifExport.setVisible(true);
        Camera cam = W.getCamera();
        expQuality.setSelectedIndex(cam.exp_quality);
        expSize.setSelectedItem(cam.exp_size);
        expResize.setSelected(cam.exp_resize);
        expOnEvent.setSelected(cam.exp_onlyevent);
        expPath.setText(cam.exp_path);
        switch (cam.exp_buffer) {
            default:
                expBuffer.setSelectedIndex(cam.exp_buffer);
                break;
            case 3:
                expBuffer.setSelectedIndex(2);
                break;
            case 5:
                expBuffer.setSelectedIndex(3);
                break;
            case 10:
                expBuffer.setSelectedIndex(4);
                break;
            case 15:
                expBuffer.setSelectedIndex(5);
                break;
            case 20:
                expBuffer.setSelectedIndex(6);
                break;
            case 30:
                expBuffer.setSelectedIndex(7);
                break;
            case 45:
                expBuffer.setSelectedIndex(8);
                break;
            case 60:
                expBuffer.setSelectedIndex(9);
                break;
        }
		/* All sizes
		160x120
		200x150
		240x180
		280x210
		320x240
		360x270
		400x300
		440x330
		480x360
		520x390
		560x420
		600x450
		640x480
		680x510
		720x540
		760x570
		800x600
		*/
    }//GEN-LAST:event_mnOptsActionPerformed

    private void mnUpdActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_mnUpdActionPerformed
        openURL("app.update");
    }//GEN-LAST:event_mnUpdActionPerformed

    private void mnDocActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_mnDocActionPerformed
        openURL("app.docs");
    }//GEN-LAST:event_mnDocActionPerformed

    private void btnExportOKActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnExportOKActionPerformed
        Camera cam = W.getCamera();
        cam.exp_quality = expQuality.getSelectedIndex();
        cam.exp_size = expSize.getSelectedItem().toString();
        cam.exp_resize = expResize.isSelected();
        cam.exp_onlyevent = expOnEvent.isSelected();
        cam.exp_path = expPath.getText();
        switch (expBuffer.getSelectedIndex()) {
            default:
                cam.exp_buffer = expBuffer.getSelectedIndex();
                break;
            case 2:
                cam.exp_buffer = 3;
                break;
            case 3:
                cam.exp_buffer = 5;
                break;
            case 4:
                cam.exp_buffer = 10;
                break;
            case 5:
                cam.exp_buffer = 15;
                break;
            case 6:
                cam.exp_buffer = 20;
                break;
            case 7:
                cam.exp_buffer = 30;
                break;
            case 8:
                cam.exp_buffer = 45;
                break;
            case 9:
                cam.exp_buffer = 60;
                break;
        }
        wDB.saveCfg(W.selCam);
        ifExport.setVisible(false);
    }//GEN-LAST:event_btnExportOKActionPerformed

    private void rSoundAlertActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_rSoundAlertActionPerformed
        rPlaySound.setEnabled(rSoundAlert.getSelectedIndex() > 0);
    }//GEN-LAST:event_rSoundAlertActionPerformed

    private void rAddRuleActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_rAddRuleActionPerformed
        rRuleList.clearSelection();
        centerFrame(ifRule);
        rRuleName.setText("ルール" + (rRuleList.getModel().getSize() + 1));

        rEventPanel.setVisible(true);
        btnLogOnly.setSelected(true);

        rMin.setText(W.props.getProperty("event.min"));
        rMinU.setSelectedIndex(0);

        rFromH.setText("00");
        rFromM.setText("00");
        rToH.setText("23");
        rToM.setText("59");
        rAlways.setSelected(true);

        rAlertPanel.setVisible(false);
        rEmail.setText("");
        rSoundAlert.setSelectedIndex(0);
        rPlaySound.setEnabled(false);
        ifRule.show();
    }//GEN-LAST:event_rAddRuleActionPerformed

    private void btnRuleOKActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnRuleOKActionPerformed
        String title = rRuleName.getText();
        wRule rule = new wRule(title);
        int unit = rMinU.getSelectedIndex();
        int multi = (unit == 0 ? 1 : (unit == 1 ? 60 : 3600));
        int min = Integer.parseInt(rMin.getText()) * multi;
        long from = (Integer.parseInt(rFromH.getText()) * 3600 + Integer.parseInt(rFromM.getText()) * 60) * 1000;
        long to = (Integer.parseInt(rToH.getText()) * 3600 + Integer.parseInt(rToM.getText()) * 60) * 1000;
        if (btnLogOnly.isSelected()) {
            if (rAlways.isSelected()) {
                rule = new wRule(title, min);
            } else {
                rule = new wRule(title, min, from, to);
            }
        } else if (btnLogAlert.isSelected()) {
            String email = rEmail.getText();
            int sound = rSoundAlert.getSelectedIndex();
            if (rAlways.isSelected()) {
                if (sound == 0) {
                    try {
                        rule = new wRule(title, min, email);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else if (sound <= 5) {
                    try {
                        rule = new wRule(title, min, email, sound);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else {
                    String soundfile = (String) rSoundAlert.getSelectedItem();
                    try {
                        rule = new wRule(title, min, email, sound, soundfile);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            } else {
                if (sound == 0) {
                    try {
                        rule = new wRule(title, min, email, from, to);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else if (sound <= 5) {
                    try {
                        rule = new wRule(title, min, email, from, to, sound);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else {
                    String soundfile = (String) rSoundAlert.getSelectedItem();
                    try {
                        rule = new wRule(title, min, email, from, to, sound, soundfile);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        }
        if (rRuleList.getSelectedIndex() == -1) {
            W.getAreas(W.selCam).get(rAreaList.getSelectedIndex()).addRule(rule);
        } else {
            W.getAreas(W.selCam).get(rAreaList.getSelectedIndex()).updRule(rRuleList.getSelectedIndex(), rule);
        }
        wDB.save(W.selCam);
        ifRule.hide();
        refreshRuleList();
        jDesktopPane.repaint(); //TODO: why? (it is not redrawing correctly)
    }//GEN-LAST:event_btnRuleOKActionPerformed

    private void rAlwaysActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_rAlwaysActionPerformed
        if (rAlways.isSelected()) {
            rFromH.setEnabled(false);
            rFromM.setEnabled(false);
            rToH.setEnabled(false);
            rToM.setEnabled(false);
        } else {
            rFromH.setText("00");
            rFromM.setText("00");
            rFromH.setEnabled(true);
            rFromM.setEnabled(true);
            rToH.setText("23");
            rToM.setText("59");
            rToH.setEnabled(true);
            rToM.setEnabled(true);
        }
    }//GEN-LAST:event_rAlwaysActionPerformed

    private void btnLogAlertActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnLogAlertActionPerformed
        rEventPanel.setVisible(true);
        rAlertPanel.setVisible(true);
    }//GEN-LAST:event_btnLogAlertActionPerformed

    private void btnLogOnlyActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnLogOnlyActionPerformed
        rEventPanel.setVisible(true);
        rAlertPanel.setVisible(false);
    }//GEN-LAST:event_btnLogOnlyActionPerformed

    private void btnNoLogActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnNoLogActionPerformed
        rEventPanel.setVisible(false);
    }//GEN-LAST:event_btnNoLogActionPerformed

    private void rRuleListMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_rRuleListMouseClicked
        int index = rRuleList.locationToIndex(evt.getPoint());
        if (rRuleList.getModel().getSize() > 0 & !rRuleList.isSelectionEmpty()) {
            centerFrame(ifRule);
            wRule rule = W.getAreas(W.selCam).get(rAreaList.getSelectedIndex()).getRule(index);

            rRuleName.setText(rule.title);

            if (rule.alert) btnLogAlert.setSelected(true);
            else if (rule.log) btnLogOnly.setSelected(true);
            else btnNoLog.setSelected(true);

            rEventPanel.setVisible(rule.log);
            rAlways.setSelected(rule.always);
            if (rule.always) {
                rFromH.setEnabled(false);
                rFromM.setEnabled(false);
                rToH.setEnabled(false);
                rToM.setEnabled(false);
                rFromH.setText("00");
                rFromM.setText("00");
                rToH.setText("23");
                rToM.setText("59");
            } else {
                rFromH.setEnabled(true);
                rFromM.setEnabled(true);
                rToH.setEnabled(true);
                rToM.setEnabled(true);
                LocalTime ltfrom = new Timestamp(rule.from).toLocalDateTime().toLocalTime();
                LocalTime ltto = new Timestamp(rule.to).toLocalDateTime().toLocalTime();
                rFromH.setText(ltfrom.format(DateTimeFormatter.ofPattern("HH")));
                rFromM.setText(ltfrom.format(DateTimeFormatter.ofPattern("mm")));
                rToH.setText(ltto.format(DateTimeFormatter.ofPattern("HH")));
                rToM.setText(ltto.format(DateTimeFormatter.ofPattern("mm")));
            }
            rAlertPanel.setVisible(rule.alert);

            int min = rule.min;
            int minu = 0;
            if (min >= 3600) {
                min = min / 3600;
                minu = 2;
            } else if (min >= 60) {
                min = min / 60;
                minu = 1;
            }

            rMin.setText(String.valueOf(min));
            rMinU.setSelectedIndex(minu);

            rEmail.setText("");
            if (rule.alert) {
                if (rule.email != null) {
                    rEmail.setText(rule.email.getAddress());
                }
                if (W.props.getBoolProperty("app.sound")) {
                    rSoundAlert.setEnabled(true);
                    rSoundAlert.setSelectedIndex(rule.sound);
                    rPlaySound.setEnabled(false);
                }
            }
            ifRule.show();
        }
    }//GEN-LAST:event_rRuleListMouseClicked

    private void rAreaListValueChanged(javax.swing.event.ListSelectionEvent evt) {//GEN-FIRST:event_rAreaListValueChanged
        refreshRuleList();
    }//GEN-LAST:event_rAreaListValueChanged

    private void rRmRuleActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_rRmRuleActionPerformed
        W.getAreas(W.selCam).get(rAreaList.getSelectedIndex()).delRule(rRuleList.getSelectedIndex());
        refreshRuleList();
        wDB.save(W.selCam);
    }//GEN-LAST:event_rRmRuleActionPerformed

    private void ifRuleComponentHidden(java.awt.event.ComponentEvent evt) {//GEN-FIRST:event_ifRuleComponentHidden
        /*ifRules.requestFocus();*/
        try { //Correct way to set focus:
            ifRules.setSelected(true);
        } catch (Exception e) {
        }
    }//GEN-LAST:event_ifRuleComponentHidden

    private void btnPlayMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnPlayMouseClicked
        vPlayer.play();
    }//GEN-LAST:event_btnPlayMouseClicked

    private void logoMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_logoMouseClicked
        openURL("app.url");
    }//GEN-LAST:event_logoMouseClicked

    private void rPlaySoundActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_rPlaySoundActionPerformed
        Sound.play(wRule.getSoundFile(rSoundAlert.getSelectedIndex()));
    }//GEN-LAST:event_rPlaySoundActionPerformed

    private void btnPauseMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnPauseMouseClicked
        vPlayer.pause();
    }//GEN-LAST:event_btnPauseMouseClicked

    private void btnFwdMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnFwdMouseClicked
        vPlayer.faster();
    }//GEN-LAST:event_btnFwdMouseClicked

    private void btnRewMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnRewMouseClicked
        vPlayer.slower();
    }//GEN-LAST:event_btnRewMouseClicked

    private void ifPlayerInternalFrameClosing(javax.swing.event.InternalFrameEvent evt) {//GEN-FIRST:event_ifPlayerInternalFrameClosing
        vPlayer.stop();
    }//GEN-LAST:event_ifPlayerInternalFrameClosing

    public static InputStream getResourceAsStream(String resource) {
        InputStream stream = WUI.class.getResourceAsStream(resource);
        if (stream == null) {
            File file = new File(System.getProperty("user.dir") + File.separator + "resources" + File.separator + resource);
            if (file.exists()) {
                try {
                    stream = new FileInputStream(file);
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                }
            } else {
                System.out.println("Unable to find resource: " + resource);
            }
        }
        return stream;
    }

    public static URL getResource(String resource) {
        URL url = WUI.class.getResource(resource);
        if (url == null) {
            File file = new File(System.getProperty("user.dir") + File.separator + "resources" + File.separator + resource);
            if (file.exists()) {
                try {
                    url = file.toURI().toURL();
                } catch (MalformedURLException e) {
                    e.printStackTrace();
                }
            } else {
                System.out.println("Unable to find resource: " + resource);
            }
        }
        return url;
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(WUI.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(WUI.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(WUI.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(WUI.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            @Override
            public void run() {
                new WUI().setVisible(true);
            }
        });
    }


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnBegin;
    private javax.swing.JButton btnCamOK;
    private javax.swing.JButton btnChangePath;
    private javax.swing.JButton btnCtrlCenter;
    private javax.swing.JButton btnCtrlContrastDown;
    private javax.swing.JButton btnCtrlContrastUp;
    private javax.swing.JButton btnCtrlDown;
    private javax.swing.JButton btnCtrlLeft;
    private javax.swing.JButton btnCtrlRight;
    private javax.swing.JButton btnCtrlUp;
    private javax.swing.JButton btnCtrlZoomIn;
    private javax.swing.JButton btnCtrlZoomOut;
    private javax.swing.JButton btnEnd;
    private javax.swing.JButton btnExportOK;
    private javax.swing.JButton btnFwd;
    private javax.swing.JToggleButton btnLogAlert;
    private javax.swing.JToggleButton btnLogOnly;
    private javax.swing.JToggleButton btnNoLog;
    private javax.swing.JButton btnPause;
    private javax.swing.JButton btnPlay;
    private javax.swing.JButton btnRew;
    private javax.swing.JButton btnRuleOK;
    private javax.swing.JComboBox camFPS;
    private javax.swing.JTextField camIP;
    private javax.swing.JCheckBox camInvert;
    private javax.swing.JComboBox camModel;
    private javax.swing.JPasswordField camPass;
    private javax.swing.JComboBox camSize;
    private javax.swing.JTextField camTitle;
    private javax.swing.JTextField camUser;
    private javax.swing.JComboBox expBuffer;
    private javax.swing.JCheckBox expOnEvent;
    private javax.swing.JTextField expPath;
    private javax.swing.JComboBox expQuality;
    private javax.swing.JCheckBox expResize;
    private javax.swing.JComboBox expSize;
    private javax.swing.Box.Filler filler1;
    private javax.swing.Box.Filler filler10;
    private javax.swing.Box.Filler filler11;
    private javax.swing.Box.Filler filler12;
    private javax.swing.Box.Filler filler2;
    private javax.swing.Box.Filler filler3;
    private javax.swing.Box.Filler filler4;
    private javax.swing.Box.Filler filler5;
    private javax.swing.Box.Filler filler6;
    private javax.swing.Box.Filler filler7;
    private javax.swing.Box.Filler filler8;
    private javax.swing.Box.Filler filler9;
    private javax.swing.ButtonGroup gbRule;
    private javax.swing.JInternalFrame ifAbout;
    private javax.swing.JInternalFrame ifCam;
    private javax.swing.JInternalFrame ifCtrl;
    private javax.swing.JInternalFrame ifExport;
    private com.intellisrc.watcher.swing.wIFLog ifLog;
    private javax.swing.JInternalFrame ifPlayer;
    private javax.swing.JInternalFrame ifRule;
    private javax.swing.JInternalFrame ifRules;
    private javax.swing.JComboBox jComboBox1;
    private javax.swing.JComboBox jComboBox2;
    private javax.swing.JDesktopPane jDesktopPane;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel15;
    private javax.swing.JLabel jLabel16;
    private javax.swing.JLabel jLabel17;
    private javax.swing.JLabel jLabel18;
    private javax.swing.JLabel jLabel19;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel20;
    private javax.swing.JLabel jLabel21;
    private javax.swing.JLabel jLabel22;
    private javax.swing.JLabel jLabel23;
    private javax.swing.JLabel jLabel24;
    private javax.swing.JLabel jLabel25;
    private javax.swing.JLabel jLabel26;
    private javax.swing.JLabel jLabel27;
    private javax.swing.JLabel jLabel28;
    private javax.swing.JLabel jLabel29;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JMenuBar jMenuBar1;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JPanel jPanel6;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JPopupMenu.Separator jSeparator1;
    private javax.swing.JSeparator jSeparator2;
    private javax.swing.JSeparator jSeparator3;
    private javax.swing.JSeparator jSeparator4;
    private javax.swing.JSeparator jSeparator6;
    private javax.swing.JSeparator jSeparator7;
    private javax.swing.JSplitPane jSplitPane1;
    private javax.swing.JToolBar jToolBar1;
    private javax.swing.JToolBar jToolBar2;
    private javax.swing.JToolBar jToolBar3;
    private javax.swing.JToolBar jToolBar4;
    private javax.swing.JToolBar jToolBar5;
    private javax.swing.JToolBar jToolBar6;
    private javax.swing.JLabel logo;
    private javax.swing.JMenuItem mnAbout;
    private javax.swing.JMenu mnArea;
    private javax.swing.JMenuItem mnAreaAdd;
    private javax.swing.JMenuItem mnAreaRm;
    private javax.swing.JMenuItem mnAreaView;
    private javax.swing.JMenu mnCam;
    private javax.swing.JMenuItem mnCamAdd;
    private javax.swing.JMenuItem mnCamRm;
    private javax.swing.JMenuItem mnCtrl;
    private javax.swing.JMenuItem mnDoc;
    private javax.swing.JMenu mnHelp;
    private javax.swing.JMenuItem mnLog;
    private javax.swing.JMenuItem mnOpts;
    private javax.swing.JMenuItem mnReset;
    private javax.swing.JMenuItem mnRule;
    private javax.swing.JMenu mnTools;
    private javax.swing.JMenuItem mnUpd;
    private javax.swing.JButton rAddRule;
    private javax.swing.JButton rAddSound;
    private javax.swing.JPanel rAlertPanel;
    private javax.swing.JCheckBox rAlways;
    private javax.swing.JList rAreaList;
    private javax.swing.JButton rBtnBrowse;
    private javax.swing.JTextField rEmail;
    private javax.swing.JPanel rEventPanel;
    private javax.swing.JTextField rExec;
    private javax.swing.JTextField rFromH;
    private javax.swing.JTextField rFromM;
    private javax.swing.JTextField rMin;
    private javax.swing.JComboBox rMinU;
    private javax.swing.JButton rPlaySound;
    private javax.swing.JButton rRmRule;
    private javax.swing.JPanel rRule;
    private javax.swing.JList rRuleList;
    private javax.swing.JTextField rRuleName;
    private javax.swing.JComboBox rSoundAlert;
    private javax.swing.JTextField rToH;
    private javax.swing.JTextField rToM;
    private javax.swing.JTextField rUrl;
    private javax.swing.JTable tLog;
    private com.intellisrc.watcher.swing.VPlayer vPlayer;
    private javax.swing.JLabel vPlayerCounter;
    private javax.swing.JLabel version;
    // End of variables declaration//GEN-END:variables
}
