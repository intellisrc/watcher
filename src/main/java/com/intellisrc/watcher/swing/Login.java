package com.intellisrc.watcher.swing;

/**
 * @author Alberto Lepe <lepe@intellisrc.com>
 */
import com.intellisrc.watcher.obj.wDB;
import org.jdesktop.swingx.auth.LoginService;

public class Login extends LoginService {

	/**
	 * @param username user name
	 * @param pass     user password
	 * @param server   server to connect
	 */
	@Override
	public boolean authenticate(final String username, char[] pass, String server) {
		String password = new String(pass);
		return wDB.login(username, password); 
	}
}