package com.intellisrc.watcher.swing;

import java.awt.Graphics;
import java.util.ArrayList;
import javax.swing.SwingConstants;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;
import javax.swing.JTable;
import javax.swing.JInternalFrame;
/**
 *
 * @author Alberto Lepe <lepe@intellisrc.com>
 */
public class wIFLog extends JInternalFrame {
	public JTable table;
	public boolean pause = false;
	private ArrayList<String[]> logs = new ArrayList();

	public String[] getLog(int lid){
		for(String[] str : logs) {
			if(str[0].equals(String.valueOf(lid))) {
				return str;
			}	
		}
		return null;	
	}

	public void setLog(int lid, String[] row) {
		for(int l = 0; l < logs.size(); l++) {
			if(logs.get(l)[0].equals(String.valueOf(lid))) {
				logs.set(l, row);
				break;
			}	
		}	
	}

	public void addLog(String[] row) {
		logs.add(row);
	}

	public void importLogs(ArrayList logs){
		this.logs = logs;
	}
	
	@Override
	public void paint(Graphics g) {
		super.paint(g);
		if(table != null &! logs.isEmpty() &! pause) { // && logcount != wLogger.get().size()) {
			DefaultTableModel model = (DefaultTableModel)table.getModel();
			model.getDataVector().removeAllElements(); 
			for(int logcount = 0; logcount < logs.size(); logcount++) {
				model.insertRow(0, logs.get(logcount));
			}
			//tLog.prepareRenderer(tLog.getCellRenderer(0, 0), 0, 0).setBackground(Color.yellow);
			DefaultTableCellRenderer headerRenderer = (DefaultTableCellRenderer) table.getTableHeader().getDefaultRenderer();
			headerRenderer.setHorizontalAlignment(SwingConstants.CENTER);
			table.repaint();
		}
	}
}
