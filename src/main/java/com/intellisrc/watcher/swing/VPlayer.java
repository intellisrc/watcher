package com.intellisrc.watcher.swing;

import com.intellisrc.watcher.cv.MJpegGrabber;
import com.intellisrc.watcher.obj.W;
import com.intellisrc.watcher.utils.ImgResizer;
import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import javax.swing.JLabel;
import javax.swing.JPanel;

/**
 *
 * @author Alberto Lepe <lepe@intellisrc.com>
 */
public class VPlayer extends JPanel {
	public String video;
	public JLabel counter;
	public BufferedImage image;
	private ScheduledExecutorService scheduler;
	private int speed = 500;
	private Runnable refresh;
	private boolean paused = false;
	private String time;
	private MJpegGrabber grabber;

	public void play(){
		if(paused) {
			paused = false;
			return;
		}
		stop();
		grabber = new MJpegGrabber(video);
		grabber.startFromFile();
		refresh = new Runnable() {
			@Override
			public void run() {
				try {
						if(!paused) {
							if(grabber != null) {
								image = grabber.readMJPGStream();
								time = grabber.time;
								repaint();
							}
						}
						if(scheduler != null) 
							scheduler.schedule(refresh, speed, java.util.concurrent.TimeUnit.MILLISECONDS);
				} catch(Exception e) {
					e.printStackTrace();
				}
			}
		};
		speed = 500 / W.getCamera().fps;
		time = "";
		scheduler = Executors.newScheduledThreadPool(1);
		scheduler.schedule(refresh, speed, java.util.concurrent.TimeUnit.MILLISECONDS);
	}

	public void faster() {
		if(speed > 100) speed -= 100;
	}
	
	public void slower() {
		if(speed < 3000) speed += 250;
	}

	public void pause() {
		paused = true;
	}

	public void stop() {
		paused = false;
		if(scheduler != null) scheduler.shutdown();
		if(image != null) {
			image.flush();
			image = null;
		}
		if(grabber != null) {
			try {
				grabber.release();
			} catch(Exception e){
				e.printStackTrace();
			}
			grabber = null;
		}
		counter.setText("00-00-00 00:00:00.000");
	}
	
	@Override
	public void paint(Graphics g) {
		if(image != null) {
			g.drawImage(ImgResizer.resize(image, getWidth(), getHeight(), ImgResizer.BEST), 0, 0, this);
			g.dispose();
			counter.setText(time);
			image.flush();
		}
	}
}
