package com.intellisrc.watcher.swing;

import com.intellisrc.watcher.obj.W;
import com.intellisrc.watcher.obj.wArea;
import com.intellisrc.watcher.obj.wDB;
import java.awt.Graphics;
import java.awt.image.BufferedImage;
import javax.swing.JPanel;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.EventQueue;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.RenderingHints;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.geom.GeneralPath;
import javax.swing.JFrame;

/**
 *
 * @author Alberto Lepe <lepe@intellisrc.com>
 */
public class VPanel extends JPanel {
	public BufferedImage image;
	private MouseHandler mouseHandler = new MouseHandler();
	private GeneralPath path = null; 
	private wArea selArea = null;
	private Point p = null;
	private Point pstart = null;
	public String newname = "";
	public int cid = -1; //Camera ID
	private boolean select = false; //when new area was created (just closed)
	private boolean edit = false; 	//edit new area
	private boolean show = true; 	//show or hide areas

	public VPanel() {
        this.addMouseListener(mouseHandler);
        this.addMouseMotionListener(mouseHandler);
	}

	public void edit(boolean start) {
		edit = start;
		if(edit) {
			this.setCursor(Cursor.getPredefinedCursor(Cursor.CROSSHAIR_CURSOR));
		} else {
			this.setCursor(Cursor.getDefaultCursor());
			pstart = null;
			path = null;
			p = null;
			newname = "";
		}
	}

	public wArea selected() {
		return selArea;
	}

	public void delete() {
		W.getAreas(cid).del(selArea);
		selArea = W.getAreas(cid).first();
	}
	
	public void showAreas() {
		show = !show;
	}
	
	@Override
	/**
	 * Paint image in Panel
	 */
	public void paint(Graphics g) {
		if (image != null) {
			Graphics2D g2d = (Graphics2D) g;
			g2d.drawImage(image, 0, 0, this);
            image.flush();
			if(show && edit && path != null) {
				g2d.setColor(new Color(250, 150, 250));
				g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING,RenderingHints.VALUE_ANTIALIAS_ON);
				g2d.setStroke(new BasicStroke(3, BasicStroke.CAP_ROUND, BasicStroke.JOIN_BEVEL));
				g2d.draw(path);
				if(select) {
					select = false;
					selArea = W.getAreas(cid).add(path,newname);
					wDB.save(cid);
					g2d.setColor(Color.black);
					g2d.drawString(newname, (int)path.getBounds2D().getCenterX(), (int)path.getBounds2D().getCenterY());
					path = null;
					newname = "";
					edit(false);
				} else {
					g2d.setColor(Color.magenta);
					g2d.drawLine((int)path.getCurrentPoint().getX(), (int)path.getCurrentPoint().getY(), p.x, p.y);		
				}
			}
			if(show) {
				if(W.getAreas(cid) != null) 
				for(int i = 0; i < W.getAreas(cid).size(); i++) { //TODO: convert to for ( .. : .. )
					wArea wa = W.getAreas(cid).get(i);
					boolean selected = false;
					if(selArea == wa) {
						float[] dashPattern = { 2, 2, 2, 2 };
						g2d.setStroke(new BasicStroke(2, BasicStroke.CAP_BUTT,BasicStroke.JOIN_MITER, 10,dashPattern, 0));
						selected = true;
					} else {
						g2d.setStroke(new BasicStroke(1, BasicStroke.CAP_ROUND, BasicStroke.JOIN_BEVEL));
					}
					g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING,RenderingHints.VALUE_ANTIALIAS_ON);
					Color outline = null;
					Color fill = null;
					int status = W.AreaStatus(cid, i);
					if(status == W.ALERT) {
						outline = Color.RED;
						fill = new Color(250, 150, 150, 50);
					} else if(status == W.LOG) {
						outline = Color.ORANGE;
						fill = new Color(150, 150, 150, 50);
					} else {
						outline = Color.GREEN;
						fill = new Color(150, 250, 150, 50);
					}
					g2d.setColor(outline);
					g2d.draw(wa.path);
					g2d.setColor(fill);
					g2d.fill(wa.path);
					g2d.setColor(Color.black);
					g2d.drawString(wa.name, (int)wa.path.getBounds2D().getCenterX(), (int)wa.path.getBounds2D().getCenterY());
				}
			}
			g2d.dispose();
		} else {
			g.clearRect(0, 0, this.getSize().width, this.getSize().height);
		}
		g.dispose();
	}
	
	private class MouseHandler extends MouseAdapter {

        @Override
        public void mouseClicked(MouseEvent e) {
			p = e.getPoint();
			if(edit) {
				if(path == null) {
					path = new GeneralPath(GeneralPath.WIND_EVEN_ODD);
					path.moveTo(p.x, p.y);
					pstart = p;
				} else {
					path.lineTo(p.x, p.y);
					if(Math.sqrt(Math.pow(p.x - pstart.x,2) + Math.pow(p.y - pstart.y,2)) < 10) {
						select = true;
					}
				}
				show = true;
			} else if(show) {
				selArea = W.getAreas(cid).search(p.x, p.y);
			}
        }

		@Override
		public void mouseMoved(MouseEvent e) {
			if(edit) {
				p = e.getPoint();	
			}
		}

    }

    private void display() {
        JFrame f = new JFrame("LinePanel");
        f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        f.add(this);
        f.pack();
        f.setLocationRelativeTo(null);
        f.setVisible(true);
    }

    public static void main(String[] args) {
        EventQueue.invokeLater(new Runnable() {

            @Override
            public void run() {
                new VPanel().display();
            }
        });
    }
}
