package com.intellisrc.watcher.swing;

import com.intellisrc.watcher.ui.WUI;

import javax.swing.JMenuItem;

/**
 *
 * @author Alberto Lepe <lepe@intellisrc.com>
 */
public class wMenuItem extends JMenuItem {
	private boolean connected = false;
	public void connect() {
		connected = true;
        setIcon(new javax.swing.ImageIcon(WUI.getResource("images/connect.png"))); // NOI18N
	}
	public void disconnect() {
		connected = false;
        setIcon(new javax.swing.ImageIcon(WUI.getResource("images/disconnect.png"))); // NOI18N
	}
	public void toggle() {
		if(connected) disconnect(); else connect();
	}
	public void setConnect(boolean connect) {
		if(connect) connect(); else disconnect();
	}
	public boolean isConnected(){
		return connected;
	}
}
