package com.intellisrc.watcher.utils;

import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.Transparency;
import java.awt.image.BufferedImage;
/**
 *
 * @author Alberto Lepe <lepe@intellisrc.com>
 * Good explanation about resizing:
 * @link: http://today.java.net/pub/a/today/2007/04/03/perils-of-image-getscaledinstance.html 
 */
public class ImgResizer {
	public static final int FAST = 1;
	public static final int SOSO = 2;
	public static final int GOOD = 3;
	public static final int BEST = 4;
	
	public static BufferedImage resize(BufferedImage img, int targetWidth, int targetHeight) {
		return resize(img, targetWidth, targetHeight, SOSO);
	}
	
	public static BufferedImage resize(BufferedImage img, int targetWidth, int targetHeight, int method) {
		Object RMethod = null;
		int w = img.getWidth();
		int h = img.getHeight();
		if(method == BEST && (targetWidth > w || targetHeight > h)) method = GOOD;
		switch(method) {
			case FAST: RMethod = RenderingHints.VALUE_INTERPOLATION_NEAREST_NEIGHBOR; break;
			default:
			case BEST:
			case SOSO: RMethod = RenderingHints.VALUE_INTERPOLATION_BILINEAR; break;
			case GOOD: RMethod = RenderingHints.VALUE_INTERPOLATION_BICUBIC; break;
		}
		if(method < BEST) {
			BufferedImage resizedImage = new BufferedImage(targetWidth, targetHeight, img.getType());
			Graphics2D g = resizedImage.createGraphics();
			g.setRenderingHint(RenderingHints.KEY_INTERPOLATION, RMethod); 
			g.drawImage(img , 0, 0, targetWidth, targetHeight, null);
			g.dispose();
			img.flush();
			return resizedImage;	
		} else { //MULTI PASS: BEST QUALITY (only when taget image is smaller)
			int type = (img.getTransparency() == Transparency.OPAQUE) ? BufferedImage.TYPE_INT_RGB : BufferedImage.TYPE_INT_ARGB;
			BufferedImage resizedImage = (BufferedImage)img;
			do { 
				if (w > targetWidth) {
					w /= 2;
					if (w < targetWidth) {
						w = targetWidth;
					}
				}
				if (h > targetHeight) {
					h /= 2;
					if (h < targetHeight) {
						h = targetHeight;
					}
				}
				BufferedImage tmp = new BufferedImage(w, h, type);
				Graphics2D g2 = tmp.createGraphics();
				g2.setRenderingHint(RenderingHints.KEY_INTERPOLATION, RMethod);
				g2.drawImage(resizedImage, 0, 0, w, h, null);
				g2.dispose();

				resizedImage = tmp;
				tmp.flush();
			} while (w != targetWidth || h != targetHeight);
				img.flush();
			return resizedImage;
		}	
	}	
}

/*	Scale 
	double scale = Math.min((double) target.getWidth() / frame.width(), (double) target.getHeight() / frame.height());
	double cHeight = scale *  frame.width();
	double cWidth = scale * frame.height();
	IplImage sframe = IplImage.create((int)cWidth,(int)cHeight,frame.depth(),frame.nChannels());
*/	
	/* Resize 
	IplImage sframe = IplImage.create(target.getWidth(),target.getHeight(),frame.depth(),frame.nChannels());
	cvResize(frame,sframe,CV_INTER_CUBIC);
	 /**/