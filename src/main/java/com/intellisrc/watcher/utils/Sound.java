package com.intellisrc.watcher.utils;

import java.io.InputStream;
import javax.sound.sampled.AudioFormat;
import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.DataLine;
import javax.sound.sampled.SourceDataLine;

/**
 * @author Alberto Lepe <lepe@intellisrc.com>
 */
public class Sound {
	private static final int EXTERNAL_BUFFER_SIZE = 524288; 
	/**
	* Play a sound file
	 * @param fileName 
	 */
	public static synchronized void play(final String fileName) {
		try {
			InputStream in = Sound.class.getResourceAsStream(fileName);
			AudioInputStream inputStream = AudioSystem.getAudioInputStream(in);
			AudioFormat format = inputStream.getFormat();
			SourceDataLine auline = null;
			DataLine.Info info = new DataLine.Info(SourceDataLine.class, format);
			auline = (SourceDataLine) AudioSystem.getLine(info);
            auline.open(format);
			auline.start();
			int nBytesRead = 0;
	        byte[] abData = new byte[EXTERNAL_BUFFER_SIZE];
			while (nBytesRead != -1) { 
                nBytesRead = inputStream.read(abData, 0, abData.length);
                if (nBytesRead >= 0) 
                    auline.write(abData, 0, nBytesRead);
            } 
			auline.drain();
			auline.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}