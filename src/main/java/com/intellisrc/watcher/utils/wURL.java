package com.intellisrc.watcher.utils;

import java.io.BufferedInputStream;
import java.io.DataInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.Authenticator;
import java.net.HttpURLConnection;
import java.net.PasswordAuthentication;
import java.net.URL;

/**
 *
 * @author Alberto Lepe <lepe@intellisrc.com>
 */
public class wURL {
	public static DataInputStream get(String url) throws Exception {
		URL u = new URL(url);
		String uinfo = u.getUserInfo();
		if(uinfo != null && uinfo.indexOf(":") != -1) {
			String[] userpass = uinfo.split(":");
			Authenticator.setDefault(new HTTPAuthenticator(userpass[0], userpass[1]));
		}
		HttpURLConnection huc = (HttpURLConnection) u.openConnection();
		InputStream is = null;
		try {
			is = huc.getInputStream();
		} catch (IOException e) { //in case no connection exists wait and try again, instead of printing the error
			try {
				huc.disconnect();
				Thread.sleep(60);
			} catch (InterruptedException ie) {
				//huc.disconnect();connect();
				e.printStackTrace();
			}
		}
		BufferedInputStream bis = new BufferedInputStream(is);
		return new DataInputStream(bis);
	}	
	
	static class HTTPAuthenticator extends Authenticator {
	    private String username, password;

	    public HTTPAuthenticator(String user, String pass) {
	      username = user;
	      password = pass;
	    }

		@Override
	    protected PasswordAuthentication getPasswordAuthentication() {
	      return new PasswordAuthentication(username, password.toCharArray());
	    }
    }	
}
