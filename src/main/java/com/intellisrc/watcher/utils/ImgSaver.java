package com.intellisrc.watcher.utils;

import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.util.Iterator;
import javax.imageio.IIOImage;
import javax.imageio.ImageWriteParam;
import javax.imageio.ImageWriter;
import javax.imageio.stream.FileImageOutputStream;
import javax.imageio.ImageIO;
/**
 *
 * @author Alberto Lepe <lepe@intellisrc.com>
 */
public class ImgSaver {
	public static void save(BufferedImage img, File file) throws Exception {
		save(img, file, 0);
	}
	
	/**
	 * Saves an image to disk
	 * @param img BufferedImage
	 * @param file File (target)
	 * @param quality int (0-100). 0 = default quality (around 80%)
	 * @throws Exception  
	 */
	public static void save(BufferedImage img, File file, int quality) throws Exception {
		String type = ext(file.getName());
		if(quality == 0)
			ImageIO.write(img, type, file); //Simple LOW QUALITY
		else {
			Iterator iter = ImageIO.getImageWritersBySuffix(type);
			ImageWriter writer = (ImageWriter)iter.next();
			ImageWriteParam iwp = writer.getDefaultWriteParam();
			iwp.setCompressionMode(ImageWriteParam.MODE_EXPLICIT);
			float q = (float)quality/100;
			iwp.setCompressionQuality(q);
			FileImageOutputStream output = new FileImageOutputStream(file);
			writer.setOutput(output);
			IIOImage image = new IIOImage(img, null, null);
			writer.write(null, image, iwp);
			writer.dispose();
		}
	}

	/**
	 * Converts image to byte[]
	 * @param img BufferedImage
	 * @param type (jpg, png)
	 * @param quality (0-100). 0 = default quality (around 80%)
	 * @return byte[]
	 * @throws Exception 
	 */
	public static byte[] convert(BufferedImage img, String type, int quality) throws Exception {
		ByteArrayOutputStream output = new ByteArrayOutputStream();
		if(quality == 0)
			ImageIO.write(img,type,output);
		else {
			Iterator iter = ImageIO.getImageWritersBySuffix(type);
			ImageWriter writer = (ImageWriter)iter.next();
			ImageWriteParam iwp = writer.getDefaultWriteParam();
			iwp.setCompressionMode(ImageWriteParam.MODE_EXPLICIT);
			float q = (float)quality/100;
			iwp.setCompressionQuality(q);
			writer.setOutput(output);
			IIOImage image = new IIOImage(img, null, null);
			writer.write(null, image, iwp);
			writer.dispose();
		}
		byte[] buff = output.toByteArray();
		output.close();
		return buff;
	}

    public static String ext(String fileName) {
		int mid= fileName.lastIndexOf(".");
		return fileName.substring(mid+1,fileName.length()).toLowerCase();  
	}
}
