package com.intellisrc.watcher.cv;

import java.awt.image.BufferedImage;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.HashMap;
import com.intellisrc.watcher.obj.wArea;
import com.intellisrc.watcher.obj.W;
import com.intellisrc.watcher.obj.CameraConfig;
import com.intellisrc.watcher.swing.VPanel;
import java.awt.Dimension;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.Executors;
import com.intellisrc.watcher.obj.wDB;
import com.intellisrc.watcher.obj.wSaver;
import org.bytedeco.javacpp.Loader;
import org.bytedeco.javacv.Frame;
import org.bytedeco.javacv.Java2DFrameConverter;
import org.bytedeco.javacv.OpenCVFrameConverter;

import static java.util.concurrent.TimeUnit.*;
import static org.bytedeco.javacpp.opencv_core.*;
import static org.bytedeco.javacpp.opencv_imgproc.*;

/**
 * @since September 19, 2011
 * @author Alberto Lepe <lepe@intellisrc.com>
 * 
 */

public class Watcher extends Thread {
	private IplImage orig = null;
    private IplImage prev = null;
    private IplImage diff = null;
    private IplImage temp = null;
    private CvMemStorage storage = null;
	private boolean run = true;
	private boolean reset = false;
	private Dimension fsize;
	private VPanel target;
	private CameraConfig cc;
	private LocalTime refresher;
	//private LocalTime resetter;
    private final ScheduledExecutorService scheduler = Executors.newScheduledThreadPool(1);
	private int secs_refresh;
	private int secs_reset;
	private int sens_width;
	private int sens_height;
	private HashMap<Integer, ArrayList> hm_activ; 
	public int cid; //CameraId
	
	public Watcher(VPanel target, CameraConfig cc) throws Exception {
		this.target = target;
		this.cc = cc;
		fsize = W.getDim(cc.camera.size);
		secs_refresh = W.props.getIntProperty("event.refresh");
		secs_reset = W.props.getIntProperty("event.reset");
		if(secs_reset != 0 && secs_refresh > secs_reset) secs_reset += secs_refresh;
		refresher = LocalTime.now();
		double sensitivity = W.props.getIntProperty("event.sensitivity")/100.00;
		sens_width = (int)(target.getWidth()*sensitivity);
		sens_height = (int)(target.getHeight()*sensitivity);
	}

	/**
	 * Quits the Video
	 */
	public void quit() {
		run = false;
		scheduler.shutdown();
	}

	/**
	 * Reset Original image
	 */
	public void reset() {
		reset = true;
	}

	/**
	 * 
	 */
	@Override
	public void start() {
		super.start();
		//Schedule Refresh every 500 milliseconds
        final Runnable refresh = new Runnable() {
			@Override
			public void run() { 
				target.repaint();
			}
        };
        scheduler.scheduleAtFixedRate(refresh, 50, 50, MILLISECONDS);
	}
	
	@Override
	public void run() {
		MJpegGrabber grabber = new MJpegGrabber(cc.camera.url.toExternalForm()+cc.grab.replace("SIZE",cc.camera.size).replace("FPS",String.valueOf(cc.camera.fps * cc.fps_mult)));
		try {
			grabber.start();
            OpenCVFrameConverter.ToIplImage iplConverter = new OpenCVFrameConverter.ToIplImage();
			IplImage frame = iplConverter.convert(grabber.grab());

			if(frame != null) {
				fsize = new Dimension(frame.width(),frame.height());
				target.setPreferredSize(fsize);
				cc.real_size = fsize;
			} else {
				frame = new IplImage(); //Empty image
			}
			while (run && (frame = iplConverter.convert(grabber.grab())) != null) {
				if(target != null) {
					if(cc.camera.invert) cvFlip(frame, null, -1); //Correct Camera Image
					if(reset) {
						orig = null;
						reset = false;
					}
					/****************** PROCESS ********************/
					//cvSmooth(frame, frame, CV_GAUSSIAN, 9, 9, 2, 2);
					if (orig == null) {
						if(storage != null) storage.release();
						storage = CvMemStorage.create();
						orig = IplImage.create(frame.width(), frame.height(), IPL_DEPTH_8U, 1);
						cvCvtColor(frame, orig, CV_RGB2GRAY);
					} else {
						if(prev != null) prev.release();
						prev = IplImage.create(frame.width(), frame.height(), IPL_DEPTH_8U, 1);
						cvCvtColor(frame, prev, CV_RGB2GRAY);
					}

					if (prev != null) {
						if (diff == null) {
							diff = IplImage.create(frame.width(), frame.height(), IPL_DEPTH_8U, 1);
						}
						// perform ABS difference
						cvAbsDiff(orig, prev, diff);
						// do some threshold for wipe away useless details
						cvThreshold(diff, diff, 64, 255, CV_THRESH_BINARY);

						// recognize contours
						CvSeq contour = new CvSeq(null);
						cvFindContours(diff, storage, contour, Loader.sizeof(CvContour.class), CV_RETR_LIST, CV_CHAIN_APPROX_SIMPLE);
						/*
						 * 
						 * 
						cvFindContours(diff, storage, contour, Loader.sizeof(CvContour.class), CV_RETR_CCOMP, CV_CHAIN_APPROX_SIMPLE);
						cvFindContours(diff, storage, contour, Loader.sizeof(CvContour.class), CV_RETR_EXTERNAL, CV_CHAIN_APPROX_TC89_KCOS);
						cvFindContours(diff, storage, contour, Loader.sizeof(CvContour.class), CV_RETR_EXTERNAL, CV_CHAIN_APPROX_SIMPLE);
						cvFindContours(diff, storage, contour, Loader.sizeof(CvContour.class), CV_RETR_TREE, CV_CHAIN_APPROX_SIMPLE); 
						cvFindContours(diff, storage, contour, Loader.sizeof(CvContour.class), CV_RETR_LIST, CV_LINK_RUNS);
						cvFindContours(diff, storage, contour, Loader.sizeof(CvContour.class), CV_RETR_LIST, CV_CHAIN_APPROX_NONE);
						cvFindContours(diff, storage, contour, Loader.sizeof(CvContour.class), CV_RETR_LIST, CV_CHAIN_APPROX_TC89_L1);
						cvFindContours(diff, storage, contour, Loader.sizeof(CvContour.class), CV_RETR_LIST, CV_CHAIN_APPROX_TC89_KCOS);
						 * 
						////cvFindContours(diff, storage, contour, Loader.sizeof(CvContour.class), CV_RETR_LIST, CV_CHAIN_CODE);
						 */
						diff.release();	
						diff = null;
						boolean found = false; //Whether any activity was registered. If true, will not update "orig" image
						while (contour != null && !contour.isNull()) {
							if (contour.elem_size() > 0) {
								CvBox2D box = cvMinAreaRect2(contour, storage);
								// test intersection
								if (box != null) {
									CvSize2D32f size = box.size();
									//cvDrawContours(frame, contour, CV_RGB(0, 100, 0), CV_RGB(0,0,0), 2, 0, 0);
									if(size.width() > sens_width && size.height() > sens_height) { 
										CvPoint2D32f center = box.center();
										if(W.getAreas(cid) != null && W.getAreas(cid).size() > 0) {
											for(int a = 0; a < W.getAreas(cid).size(); a++) {  
												wArea wa = W.getAreas(cid).get(a);
												if(wa.path.contains(center.x(),center.y())) {
													if(W.AreaStatus(cc.cid, a) > W.NOLOG) {
														found = true;
														W.setEvent(cc.cid, a, toBufferedImage(frame));
														cvDrawCircle(frame, cvPoint((int)center.x(), (int)center.y()), Math.min((int)size.height(),(int)size.width()), CV_RGB(255, 0, 0), 2, CV_AA, 0); //alias of: cvCircle
													} else {
														//cvDrawCircle(frame, cvPoint((int)center.x(), (int)center.y()), Math.min((int)size.height(),(int)size.width()), CV_RGB(0, 255, 0), 2, CV_AA, 0); //alias of: cvCircle
													}
												} else {
													W.noEvent(cc.cid, a, toBufferedImage(frame));
												}
											}
										} else { //In case no areas are set, it will use the whole screen
											//found = true;
											//W.setEvent(cc.cid, -1); //TODO
											//cvDrawCircle(frame, cvPoint((int)center.x(), (int)center.y()), (int)size.height(), CV_RGB(255, 0, 0), 2, CV_AA, 0); //alias of: cvCircle
										}
									}
								}
								box.deallocate();
							}
							contour = contour.h_next();
						}
						if(!found) {
							if(refresher.plusSeconds(secs_refresh).isBefore(LocalTime.now())) { //Every N seconds
								orig.release();
								orig = null;
								refresher = LocalTime.now();
								//resetter = null;
								if((int)(Math.random()*1000) == 1000) { //Clean: around 2 times a day (with no activity)
									wDB.cleanLog(7);
									wSaver.clean(7);
								}
								System.gc(); 
							}
						} else {
							refresher = LocalTime.now();
						}
					}
					target.image = toBufferedImage(frame);
				}
				/***************** END PROCESS *****************/
				frame.release();
			}
			frame.release();
			grabber.stop();
            target.image.flush();
			target.image = null;
			storage.release();
		} catch(Exception e){
			e.printStackTrace();
		}
	}

    /**
     * Convert IplImage to BufferedImage
     * @param src
     * @return
     */
    private static BufferedImage toBufferedImage(IplImage src) {
        OpenCVFrameConverter.ToIplImage iplConverter = new OpenCVFrameConverter.ToIplImage();
        Java2DFrameConverter bimConverter = new Java2DFrameConverter();
        Frame frame = iplConverter.convert(src);
        BufferedImage img = bimConverter. convert(frame);
        BufferedImage result = (BufferedImage)img.getScaledInstance(
                img.getWidth(), img.getHeight(), java.awt.Image.SCALE_DEFAULT);
        img.flush();
        return result;
    }
}
