package com.intellisrc.watcher.cv;

import com.intellisrc.watcher.utils.wURL;
import org.bytedeco.javacv.Frame;
import org.bytedeco.javacv.FrameGrabber;
import org.bytedeco.javacv.Java2DFrameConverter;

import java.awt.image.BufferedImage;
import java.io.*;
import java.nio.ByteBuffer;
import javax.imageio.ImageIO;

/**
 *
 * @author Alberto Lepe <lepe@intellisrc.com>
 */
public class MJpegGrabber extends FrameGrabber {
	private DataInputStream dis;
	private boolean connected = false;
	private String mjpgURL = "";
	private String token;
	public String time;

	public MJpegGrabber (String URI) {
		mjpgURL = URI;	
	}
	
	@Override
    public void release() throws Exception {
        stop();
    }

	@Override
    public void start() throws Exception {
		try {
			dis = wURL.get(mjpgURL);
		} catch (java.lang.Exception e) {
			e.printStackTrace();
		}
		connected = true;
    }

	@Override
    public void stop() throws Exception {
		try {
			dis.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		connected = false;
    }

	@Override
    public void trigger() throws Exception {
    }

	public void startFromFile() {
		try {
		  	FileInputStream fin = new FileInputStream(mjpgURL);
		  	dis = new DataInputStream(fin);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
	}
	
    public Frame grab() throws Exception {
		if(!connected) throw new Exception("Not Connected. Was it started?");
		BufferedImage image = null;
		try {
			image = readMJPGStream();
		} catch (IOException e) {
			e.printStackTrace();
		}
		//Create an RGB type image so colors can be shown correctly
		//IplImage image = IplImage.createFrom(image); <-- next code replaces this line
	    int colLimit = image.getWidth(); 
        int rowLimit = image.getHeight(); 
	    BufferedImage kernelised = new BufferedImage(colLimit, rowLimit, BufferedImage.TYPE_INT_RGB); //TYPE_INT_ARGB
		kernelised.getGraphics().drawImage(image, 0, 0, null); 
		image.flush();
		Java2DFrameConverter java2dConverter = new Java2DFrameConverter();
		Frame img = java2dConverter.convert(kernelised);
		kernelised.flush();
		return img;
    }

	public BufferedImage readMJPGStream() throws IOException {
		String curLine = readLine();
		if(token == null) token = curLine;
		while(!token.equals(curLine)) {
			curLine = readLine();
		}
		int size = Integer.parseInt(readLine(true)); //Size
		readLine(); //Content-type
		time = readLine(); //Empty line
		ByteBuffer bb = ByteBuffer.allocate(size);
		byte b;
		while(size > 0) {
			b = dis.readByte();
			bb.put(b);
			size--;
		}
		InputStream is = new ByteArrayInputStream(bb.array());
		readLine(); //Empty line
		return ImageIO.read(is);
	}
	
	private String readLine() throws IOException {
		return readLine(false);
	}
	private String readLine(boolean numbersOnly) throws IOException {
		boolean end = false;
		String lineEnd = "\n"; //assumes that the end of the line is marked with this
		byte[] lineEndBytes = lineEnd.getBytes();
		byte[] byteBuf = new byte[lineEndBytes.length];
		String line = "";
		
		while (!end) {
			dis.read(byteBuf, 0, lineEndBytes.length);
			String t = new String(byteBuf);
			if (t.equals(lineEnd)) {
				end = true;
			} else if(numbersOnly){
				if(t.codePointAt(0) > 47 && t.codePointAt(0) < 58) line = line+t;
			} else {
				line = line+t;
			}
		}
		return line;
	}	
	
}