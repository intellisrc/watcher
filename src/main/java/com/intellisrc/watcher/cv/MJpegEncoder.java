package com.intellisrc.watcher.cv;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
/**
 *
 * @author Alberto Lepe <lepe@intellisrc.com>
 */
public class MJpegEncoder {
	FileOutputStream mjpeg;
	
	public MJpegEncoder(String output) throws FileNotFoundException{
	    mjpeg = new FileOutputStream(new File(output));
	}
	
	public void export(byte[] data, String time) throws IOException {
		mjpeg.write((
			"--BoundaryString\r\n" +
			"Content-Length: " + data.length + "\r\n" +
			"Content-type: image/jpg\r\n" +
			time + "\r\n").getBytes());
		mjpeg.write(data);
		mjpeg.write("\r\n\r\n".getBytes());
		mjpeg.flush();
	}
}