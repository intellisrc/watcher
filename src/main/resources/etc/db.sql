DROP ALL OBJECTS;

CREATE  TABLE IF NOT EXISTS `kamkonf` (
  `kid` TINYINT(4) NOT NULL,
  `fps_mult` SMALLINT(1) DEFAULT 1, /* IF USE MILLISECONDS MULTIPLY PER 1000 */
  `def_user` VARCHAR(10) NOT NULL,
  `def_pass` VARCHAR(10) NOT NULL,
  `resolutions` VARCHAR(255) NULL DEFAULT NULL ,
  `grab` VARCHAR(255) NULL DEFAULT NULL ,
  `up` VARCHAR(255) NULL DEFAULT NULL ,
  `down` VARCHAR(255) NULL DEFAULT NULL ,
  `left` VARCHAR(255) NULL DEFAULT NULL ,
  `right` VARCHAR(255) NULL DEFAULT NULL ,
  `home` VARCHAR(255) NULL DEFAULT NULL ,
  `in` VARCHAR(255) NULL DEFAULT NULL ,
  `out` VARCHAR(255) NULL DEFAULT NULL ,
  `nightvision_on` VARCHAR(255) NULL DEFAULT NULL ,
  `nightvision_off` VARCHAR(255) NULL DEFAULT NULL ,
  `contrastp` VARCHAR(255) NULL DEFAULT NULL ,
  `contrastm` VARCHAR(255) NULL DEFAULT NULL ,
  `backlight_on` VARCHAR(255) NULL DEFAULT NULL ,
  `backlight_off` VARCHAR(255) NULL DEFAULT NULL ,
  PRIMARY KEY (`kid`) );

CREATE  TABLE IF NOT EXISTS `models` (
  `mid` SMALLINT(6) NOT NULL,
  `kid` TINYINT(4) NOT NULL ,
  `model` VARCHAR(255) NULL DEFAULT NULL,
  PRIMARY KEY (`mid`, `kid`) ,
  CONSTRAINT `fk_model_kamkonf1`
    FOREIGN KEY (`kid` )
    REFERENCES `kamkonf` (`kid` )
    ON DELETE CASCADE
    ON UPDATE CASCADE);

CREATE  TABLE IF NOT EXISTS `users` (
  `user` VARCHAR(15) NULL DEFAULT NULL ,
  `pass` VARCHAR(255) NULL DEFAULT NULL,
  `defcam` TINYINT(4) NULL DEFAULT NULL);

CREATE  TABLE IF NOT EXISTS `cams` (
  `cid` TINYINT(4) NOT NULL AUTO_INCREMENT ,
  `mid` TINYINT(4) NOT NULL ,
  `title` VARCHAR(50) NOT NULL ,
  `areas` OTHER NULL ,
  `config` OTHER NULL ,
  PRIMARY KEY (`cid`) ,
  CONSTRAINT `fk_model_cams1`
    FOREIGN KEY (`mid` )
    REFERENCES `models` (`mid` )
    ON DELETE CASCADE
    ON UPDATE CASCADE);

CREATE TABLE IF NOT EXISTS `logs` (
  `lid` INT NOT NULL AUTO_INCREMENT,
  `start` TIMESTAMP NOT NULL,
  `stop` TIMESTAMP NOT NULL,
  `cid` TINYINT(4) NOT NULL,
  `aid` TINYINT(4) NOT NULL,
  `rid` TINYINT(4) NOT NULL,
  PRIMARY KEY (`lid`) ,
  CONSTRAINT `fk_cams_logs1`
    FOREIGN KEY (`cid` )
    REFERENCES `cams` (`cid` )
    ON DELETE CASCADE
    ON UPDATE CASCADE);

/*`resolutions` = '192x144,320x240,640x480,1280x960,1280x1024',*/
/*`resolutions` = '320x180,320x240,640x360,640x480,800x600,1280x720,1280x960,1280x1024',*/
INSERT INTO `kamkonf` SET `kid` = 1,
  `def_user` = 'admin',
  `def_pass` = '12345',
  `fps_mult` = 1,
  `resolutions` = '320x240,640x480,1280x960',
  `grab` = 'nphMotionJpeg?Resolution=SIZE&Quality=Clarity&Framerate=FPS',
  `up` = 'nphControlCamera?Direction=TiltUp',
  `down` = 'nphControlCamera?Direction=TiltDown',
  `left` = 'nphControlCamera?Direction=PanLeft',
  `right` = 'nphControlCamera?Direction=PanRight',
  `home` = 'nphControlCamera?Direction=HomePosition',
  `in` = 'Set?Func=Zoom&Kind=0&ZoomMode=6',
  `out` = 'Set?Func=Zoom&Kind=0&ZoomMode=4',
  `nightvision_on` = 'Set?Func=NightView&Kind=1&Data=1',
  `nightvision_off` = 'Set?Func=NightView&Kind=1&Data=0',
  `contrastp` = 'nphControlCamera?Direction=Brighter',
  `contrastm` = 'nphControlCamera?Direction=Darker',
  `backlight_on` = 'Set?Func=BackLight&Kind=1&Data=1',
  `backlight_off` = 'Set?Func=BackLight&Kind=1&Data=0'
;
/** FPMS means frames per each 1000 seconds **/
INSERT INTO `kamkonf` SET `kid` = 5,
  `def_user` = 'root',
  `def_pass` = 'VB-C60',
  `fps_mult` = 1000,
  `resolutions` = '320x240,640x480',
  `grab` = '-wvhttp-01-/video.cgi?v=jpg:SIZE:5:FPS', 
  `up` = '-wvhttp-01-/control.cgi?tilt=d+500', 
  `down` = '-wvhttp-01-/control.cgi?tilt=d-500',
  `left` = '-wvhttp-01-/control.cgi?pan=d-1000',
  `right` = '-wvhttp-01-/control.cgi?pan=d+1000',
  `home` = '-wvhttp-01-/control.cgi?pan=0&tilt=0',
  `in` = '-wvhttp-01-/control.cgi?zoom=d-500',
  `out` = '-wvhttp-01-/control.cgi?zoom=d+500',
  `nightvision_on` = '-wvhttp-01-/control.cgi?dn=1',
  `nightvision_off` = '-wvhttp-01-/control.cgi?dn=0',
  `contrastp` = '-wvhttp-01-/control.cgi?ae.brightness=2',
  `contrastm` = '-wvhttp-01-/control.cgi?ae.brightness=-2',
  `backlight_on` = '-wvhttp-01-/control.cgi?shade=1',
  `backlight_off` = '-wvhttp-01-/control.cgi?shade=0'
;
/*
INSERT INTO `models` VALUES(1,1,'BB-HCM100');
INSERT INTO `models` VALUES(2,1,'BB-HCM371');
*/
INSERT INTO `models` VALUES(3,1,'BB-HCM511'); /*B*/
INSERT INTO `models` VALUES(4,1,'BB-HCM515'); /*B*/
INSERT INTO `models` VALUES(5,1,'BB-HCM527'); /*C*/
INSERT INTO `models` VALUES(6,1,'BB-HCM531'); /*B*/
INSERT INTO `models` VALUES(7,1,'BB-HCM547'); /*C*/
INSERT INTO `models` VALUES(8,1,'BB-HCM580'); /*C*/
INSERT INTO `models` VALUES(9,1,'BB-HCM581'); /*C*/
INSERT INTO `models` VALUES(10,1,'BB-HCM701'); /*B*/
INSERT INTO `models` VALUES(11,1,'BB-HCM705'); /*A*/
INSERT INTO `models` VALUES(12,1,'BB-HCM715'); /*A*/
INSERT INTO `models` VALUES(13,1,'BB-HCM735'); /*A*/
INSERT INTO `models` VALUES(14,1,'BB-SC384');
INSERT INTO `models` VALUES(15,1,'BB-SC385'); /*D*/
INSERT INTO `models` VALUES(16,1,'BB-SW352'); /*E*/
INSERT INTO `models` VALUES(17,1,'BB-SW355'); /*E*/
INSERT INTO `models` VALUES(18,1,'BB-SW395'); /*D*/
INSERT INTO `models` VALUES(30,5,'VB-C60');

INSERT INTO `users` VALUES('demo',HASH('SHA256',STRINGTOUTF8('demo'),10),NULL);

/*NOTES
B: has no zoom-in/out through ?Func=Zoom
*/
